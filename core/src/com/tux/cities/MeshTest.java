package com.tux.cities;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.FPSLogger;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Mesh;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.PerspectiveCamera;
import com.badlogic.gdx.graphics.VertexAttribute;
import com.badlogic.gdx.graphics.VertexAttributes;
import com.badlogic.gdx.graphics.g3d.Material;
import com.badlogic.gdx.graphics.g3d.Model;
import com.badlogic.gdx.graphics.g3d.Renderable;
import com.badlogic.gdx.graphics.g3d.Shader;
import com.badlogic.gdx.graphics.g3d.model.NodePart;
import com.badlogic.gdx.graphics.g3d.model.data.ModelData;
import com.badlogic.gdx.graphics.g3d.shaders.DefaultShader;
import com.badlogic.gdx.graphics.g3d.utils.CameraInputController;
import com.badlogic.gdx.graphics.g3d.utils.DefaultShaderProvider;
import com.badlogic.gdx.graphics.g3d.utils.DefaultTextureBinder;
import com.badlogic.gdx.graphics.g3d.utils.ModelBuilder;
import com.badlogic.gdx.graphics.g3d.utils.RenderContext;
import com.badlogic.gdx.graphics.glutils.ShaderProgram;
import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.utils.GdxRuntimeException;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;


public class MeshTest extends ApplicationAdapter {

	public PerspectiveCamera cam;
	public CameraInputController camController;
	public Shader shader;
	public RenderContext renderContext;
	public Model model;
	public Renderable renderable;

	public ShaderProgram shaderProgram;
	Mesh mesh;
	private int lastWidth;
	private int lastHeight;


	@Override
	public void create() {

		cam = new PerspectiveCamera(90, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
		cam.position.set(0f, 0f, 0f);
		cam.lookAt(0, 0, 1f);
//		cam.near = 1f;
		cam.near = 0.1f;
		cam.far = 3000f;
		cam.update();

		camController = new CameraInputController(cam);
		Gdx.input.setInputProcessor(camController);


		ModelBuilder modelBuilder = new ModelBuilder();

		model = modelBuilder.createSphere(2f, 2f, 2f, 20, 20,
				new Material(),
				VertexAttributes.Usage.Position | VertexAttributes.Usage.Normal | VertexAttributes.Usage.TextureCoordinates);

		NodePart blockPart = model.nodes.get(0).parts.get(0);

		renderable = new Renderable();
		renderable.environment = null;
		renderable.worldTransform.idt();

		blockPart.setRenderable(renderable);


		renderContext = new RenderContext(new DefaultTextureBinder(DefaultTextureBinder.WEIGHTED, 1));

		mesh = null;
		if (mesh == null) {
			mesh = new Mesh(true, 3, 3,
					new VertexAttribute(VertexAttributes.Usage.Position, 3, "a_position"));

			mesh.setVertices(new float[] { 5f, 5f, 1.0f,
					-5f, -5f, 1.0f,
					0, 0.5f, 1.0f });

			mesh.setIndices(new short[] { 0, 1, 2 });
		}


//		shader = new TestShader();
		shader = new DefaultShader(renderable);

		shader.init();

		shaderProgram = new DefaultShader(renderable).program;
	}


	@Override
	public void render () {
		camController.update();

		Matrix4 movemat = new Matrix4();
		movemat.translate(0.00f, 0.00f, 0.03f);

		model.meshes.get(0).transform(movemat);

		Gdx.gl.glViewport(0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT);

		renderContext.begin();

		shader.begin(cam, renderContext);
		shader.render(renderable);

		shader.end();

		renderContext.end();


	}


	@Override
	public void resize(int width, int height) {

	}

	@Override
	public void dispose () {

	}

}
