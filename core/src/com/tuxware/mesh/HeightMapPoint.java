package com.tuxware.mesh;

class HeightMapPoint {

    int x;
    int y;

    float amplitude;

    public HeightMapPoint(int x, int y, float amplitude) {
        this.x = x;
        this.y = y;
        this.amplitude = amplitude;
    }

}
