package com.tuxware.mesh;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Vector3;

public class GeometryOperations {


    public static KirraArrayList<Triangle> copyTriangles(KirraArrayList<Triangle> triangles, Color color) {
        KirraArrayList<Triangle> trianglesCopy = new KirraArrayList<Triangle>(triangles.size());

        for(int i = 0; i < triangles.size(); i++) {
            Triangle triangle = new Triangle(triangles.get(i));
            triangle.color = color;
            trianglesCopy.add(triangle);
        }

        return trianglesCopy;
    }

    public static void translateAllTriangles(KirraArrayList<Triangle> triangles, float dx, float dy, float dz) {
        for (int j = 0; j < triangles.size(); j++) {
            triangles.get(j).p1.x += dx;
            triangles.get(j).p1.y += dy;
            triangles.get(j).p1.z += dz;

            triangles.get(j).p2.x += dx;
            triangles.get(j).p2.y += dy;
            triangles.get(j).p2.z += dz;

            triangles.get(j).p3.x += dx;
            triangles.get(j).p3.y += dy;
            triangles.get(j).p3.z += dz;

        }
    }

    public static void scaleAllTriangles(KirraArrayList<Triangle> triangles, float scale) {
        for(int i = 0; i < triangles.size(); i++) {
            triangles.get(i).p1.scl(scale);
            triangles.get(i).p2.scl(scale);
            triangles.get(i).p3.scl(scale);
        }
    }

    public static void rotateAllTriangles(KirraArrayList<Triangle> edgeRectangles, float rotationAngle, Vector3 rotationAxis) {
        for (int j = 0; j < edgeRectangles.size(); j++) {
            edgeRectangles.get(j).p1.rotate(rotationAxis, rotationAngle);
            edgeRectangles.get(j).p2.rotate(rotationAxis, rotationAngle);
            edgeRectangles.get(j).p3.rotate(rotationAxis, rotationAngle);
        }
    }

}
