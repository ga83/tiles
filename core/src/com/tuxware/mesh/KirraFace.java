package com.tuxware.mesh;

public class KirraFace {

    public KirraArrayList<Integer> vertexPositionIndices;
    public KirraArrayList<Integer> vertexNormalIndices;
    public KirraArrayList<Integer> vertexTextureCoordinateIndices;

    public KirraFace(
            KirraArrayList<Integer> vertexPositionIndices,
            KirraArrayList<Integer> vertexNormalIndices,
            KirraArrayList<Integer> vertexTextureCoordinateIndices) {

        this.vertexPositionIndices = vertexPositionIndices;
        this.vertexNormalIndices = vertexNormalIndices;
        this.vertexTextureCoordinateIndices = vertexTextureCoordinateIndices;
    }

}
