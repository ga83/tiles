package com.tuxware.mesh;

public class PerlinColorer extends Colorer {

    public PerlinColorer(String colorScheme) {
        super(colorScheme);

        heightMap = new PerlinHeightMap(26 * 2, 26 * 2);
        heightMap.generate();

        for(int i = 0; i < PoolsHeightMap.NUM_COLORS; i++) {
            colorComponentsArray[i] = Color.getRandomColorFromScheme(colorScheme, false);
        }

    }


    public float[] getColor(float x, float y) {
        int height = (int) heightMap.getHeightMap()[26 + (int)x][26 + (int)y];
        float[] colorComponents = colorComponentsArray[height];
        return colorComponents;
    }
}
