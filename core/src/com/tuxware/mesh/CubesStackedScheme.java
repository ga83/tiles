package com.tuxware.mesh;

import com.badlogic.gdx.math.Vector3;

import java.util.ArrayList;

/**
 * Multi-layer hexagon / honeycomb pattern like KDE wallpaper has.
 */
//TODO: must implement some type of culling for faces, both for backward facing ones, and ones nowhere near on screen. very battery intensive at the moment.
public class CubesStackedScheme implements MeshScheme {

    private static KirraArrayList<Triangle> triangles = TriangleListSingleton.triangles;

    public static final int PERLIN_VALUE_MULTIPLIER = 4;
    public static final int PERLIN_PERIOD = 6000;

    private static final float Z_COORDINATE_BASE    = 3500.0f;
    private static final float Z_COORDINATE_TOP     = 2500.0f;
    private static final float Z_DISC_HEIGHT = Z_COORDINATE_BASE - Z_COORDINATE_TOP;

    private static int SIDE_LENGTH = 1000;


    @Override
    public KirraArrayList<Triangle> create(float filled, Colorer colorer, TileGenerator tileGenerator, float margin) {

        triangles.clear();

        float perlinOffsetX = MeshWallpaper.rnd.nextFloat() * 100;
        float perlinOffsetY = MeshWallpaper.rnd.nextFloat() * 100;

        for(int row = -26; row < 26; row++) {
            for (int column = -26; column < 26; column++) {

                float xcentre = SIDE_LENGTH * column;
                float ycentre = SIDE_LENGTH * row;

                // get height from perlin noise
                double px;
                double py;

                px = xcentre / PERLIN_PERIOD;
                py = ycentre / PERLIN_PERIOD;

                px += perlinOffsetX;
                py += perlinOffsetY;

                // get height from perlin noise
                float perlinValueFloat = (float) (ImprovedNoise.noise(px, py, 0.0));

                // add 1 before we multiply to make sure we have no negatives
                perlinValueFloat += 1.0f;

                int totalDiscHeight = (int)(perlinValueFloat * PERLIN_VALUE_MULTIPLIER) - 3;

                totalDiscHeight = Math.max(totalDiscHeight,0);

                for(int currentDiscHeight = 0; currentDiscHeight < totalDiscHeight; currentDiscHeight++) {
                    generateCube(xcentre, ycentre, currentDiscHeight, totalDiscHeight, colorer, filled, column, row);
                }
            }
        }

        return triangles;
    }

    private void generateCube(float xcentre, float ycentre, int discHeight, int totalDiscHeight, Colorer colorer, float filled, int column, int row) {

        float baseZ = Z_DISC_HEIGHT * -discHeight;

        float margin = 10;

        // base vertices
        Vector3 baseTopLeft     = new Vector3(-SIDE_LENGTH / 2 + xcentre + margin,  SIDE_LENGTH / 2 + ycentre - margin, Z_COORDINATE_BASE + baseZ);
        Vector3 baseTopRight    = new Vector3( SIDE_LENGTH / 2 + xcentre - margin,  SIDE_LENGTH / 2 + ycentre - margin, Z_COORDINATE_BASE + baseZ);
        Vector3 baseBottomLeft  = new Vector3(-SIDE_LENGTH / 2 + xcentre + margin, -SIDE_LENGTH / 2 + ycentre + margin, Z_COORDINATE_BASE + baseZ);
        Vector3 baseBottomRight = new Vector3( SIDE_LENGTH / 2 + xcentre - margin, -SIDE_LENGTH / 2 + ycentre + margin, Z_COORDINATE_BASE + baseZ);

        // top vertices
        Vector3 roofTopLeft     = baseTopLeft.cpy();        roofTopLeft.z = Z_COORDINATE_TOP + baseZ;
        Vector3 roofTopRight    = baseTopRight.cpy();       roofTopRight.z = Z_COORDINATE_TOP + baseZ;
        Vector3 roofBottomLeft  = baseBottomLeft.cpy();     roofBottomLeft.z = Z_COORDINATE_TOP + baseZ;
        Vector3 roofBottomRight = baseBottomRight.cpy();    roofBottomRight.z = Z_COORDINATE_TOP + baseZ;

        float[] colorComponents;

        if(MeshWallpaper.rnd.nextFloat() < filled) {
            colorComponents = colorer.getColor(column, row);
        } else {
            colorComponents = Color.NONFILLED;
        }

        com.badlogic.gdx.graphics.Color color = new com.badlogic.gdx.graphics.Color(colorComponents[0], colorComponents[1], colorComponents[2], 1f);

        Vector3 roofCentre = new Vector3(xcentre, ycentre, Z_COORDINATE_TOP + baseZ);

        Triangle triangle;

        // top side
        triangle = new Triangle(baseTopLeft.cpy(), baseTopRight.cpy(), roofTopLeft.cpy(), color, baseTopLeft.cpy().sub(roofCentre));
        triangles.add(triangle);

        triangle = new Triangle(baseTopRight.cpy(), roofTopRight.cpy(), roofTopLeft.cpy(), color, baseTopRight.cpy().sub(roofCentre));
        triangles.add(triangle);

        // right side
        triangle = new Triangle(baseTopRight.cpy(), baseBottomRight.cpy(), roofTopRight.cpy(), color, baseTopRight.cpy().sub(roofCentre));
        triangles.add(triangle);

        triangle = new Triangle(baseBottomRight.cpy(), roofBottomRight.cpy(), roofTopRight.cpy(), color, baseBottomRight.cpy().sub(roofCentre));
        triangles.add(triangle);

        // bottom side
        triangle = new Triangle(baseBottomRight.cpy(), baseBottomLeft.cpy(), roofBottomRight.cpy(), color, baseBottomRight.cpy().sub(roofCentre));
        triangles.add(triangle);

        triangle = new Triangle(baseBottomLeft.cpy(), roofBottomLeft.cpy(), roofBottomRight.cpy(), color, baseBottomLeft.cpy().sub(roofCentre));
        triangles.add(triangle);

        // left side
        triangle = new Triangle(baseBottomLeft.cpy(), baseTopLeft.cpy(), roofBottomLeft.cpy(), color, baseBottomLeft.cpy().sub(roofCentre));
        triangles.add(triangle);

        triangle = new Triangle(baseTopLeft.cpy(), roofTopLeft.cpy(), roofBottomLeft.cpy(), color, baseTopLeft.cpy().sub(roofCentre));
        triangles.add(triangle);

        // only draw the top if it's the top disc of the stack
        if(discHeight == totalDiscHeight - 1 && totalDiscHeight > 0) {
            // roof
            triangle = new Triangle(roofTopLeft.cpy(), roofTopRight.cpy(), roofBottomLeft.cpy(), color);
            triangles.add(triangle);

            triangle = new Triangle(roofTopRight.cpy(), roofBottomRight.cpy(), roofBottomLeft.cpy(), color);
            triangles.add(triangle);
        }
    }

    @Override
    public KirraArrayList<Triangle> update(float x, float y) {

        return null;
    }
}
