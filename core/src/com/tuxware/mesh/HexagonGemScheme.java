package com.tuxware.mesh;

import com.badlogic.gdx.math.Vector3;

import java.util.ArrayList;

/**
 * This is more like a "honeycomb" with flattened hexagons meant to be viewed at high zoom level, with say, 10 or less hexagons visible at once. And no elevation differences.
 */
public class HexagonGemScheme implements MeshScheme {

    private static KirraArrayList<Triangle> triangles = TriangleListSingleton.triangles;

    private static final float Z_COORDINATE_BASE    = 1070.0f;
    private static final float Z_COORDINATE_TOP = 1000.0f;

    private static final float HEXAGON_CIRCUMCIRCLE_RADIUS_OR_SIDE_LENGTH = 1000.0f / 1;     // from the centre to one of the vertices
    private static final float HEXAGON_INCIRCLE_RADIUS = 866.0f / 1; // not right, just guessing

    private static final float TOP_LENGTH_PERCENTAGE = 0.75f; // the proportion of the base's edge lengths to use for the top's edge lengths


    @Override
    public KirraArrayList<Triangle> create(float filled, Colorer colorer, TileGenerator tileGenerator, float margin) {

        triangles.clear();

        for(int column = -26; column < 26; column++) {
            for (int row = -26; row < 26; row++) {

                float widthTakenPerHexagon = (float) ((HEXAGON_CIRCUMCIRCLE_RADIUS_OR_SIDE_LENGTH * 2) - Math.cos(Math.toRadians(60.0)) * HEXAGON_CIRCUMCIRCLE_RADIUS_OR_SIDE_LENGTH);

                float verticalPadding = column % 2 == 0 ? HEXAGON_INCIRCLE_RADIUS: 0;
                float xcentre = column * widthTakenPerHexagon;
                float ycentre = row * HEXAGON_INCIRCLE_RADIUS * 2 + verticalPadding;

                generateHexagonGem(xcentre, ycentre, colorer, filled, column, row, tileGenerator, margin);
            }
        }
        return triangles;
    }

    private void generateHexagonGem(float xcentre, float ycentre, Colorer colorer, float filled, int column, int row, TileGenerator tileGenerator, float margin) {

       float[] colorComponents;

       if(MeshWallpaper.rnd.nextFloat() < filled) {
           colorComponents = colorer.getColor(column, row);
       } else {
            colorComponents = Color.NONFILLED;
       }

       com.badlogic.gdx.graphics.Color color = new com.badlogic.gdx.graphics.Color(colorComponents[0], colorComponents[1], colorComponents[2], 1f);
       Vector3 centre = new Vector3(xcentre, ycentre, 0);
       KirraArrayList<Triangle> triangles = tileGenerator.generateTile(HEXAGON_CIRCUMCIRCLE_RADIUS_OR_SIDE_LENGTH, centre, 6, color, RotateExtra.NONE, margin);
       this.triangles.addAll(triangles);
    }

    @Override
    public KirraArrayList<Triangle> update(float x, float y) {
        return null;
    }
}
