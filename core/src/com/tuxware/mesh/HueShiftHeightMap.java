package com.tuxware.mesh;

import com.badlogic.gdx.math.Vector2;

public class HueShiftHeightMap implements HeightMap {

    public static final int NUM_COLORS = 100;

    private final int width;
    private final int height;

    public double[][] heightMap;


    @Override
    public void generate() {

        for(int row = 0; row < height; row++) {
            for(int column = 0; column < width; column++) {

                double heightMapValue;

                heightMapValue = ImprovedNoise.noise(column / 10f, row / 10f, 0) * 1000;

                heightMap[row][column] = heightMapValue;
            }
        }

        double maximum = Double.MIN_VALUE;
        double minimum = Double.MAX_VALUE;

        for(int row = 0; row < height; row++) {
            for (int column = 0; column < width; column++) {

                if(heightMap[row][column] > maximum) {
                    maximum = heightMap[row][column];
                }

                if(heightMap[row][column] < minimum) {
                    minimum = heightMap[row][column];
                }
            }
        }

        double normaliseFactor;
        double maximumDistanceFromZero = maximum - 0;
        double minimumDistanceFromZero = 0 - minimum;

        if(maximumDistanceFromZero > minimumDistanceFromZero) {
            normaliseFactor = maximumDistanceFromZero / ((NUM_COLORS - 1) / 2);
        } else {
            normaliseFactor = minimumDistanceFromZero / ((NUM_COLORS - 1) / 2);
        }

        for(int row = 0; row < height; row++) {
            for (int column = 0; column < width; column++) {
                heightMap[row][column] /= normaliseFactor;
                heightMap[row][column] += NUM_COLORS / 2;
            }
        }
    }


    public double[][] getHeightMap() {
        return this.heightMap;
    }


    public HueShiftHeightMap(int width, int height) {
        this.width = width;
        this.height = height;

        heightMap = new double[width][height];
    }

}
