package com.tuxware.mesh;


import java.util.ArrayList;

public interface MeshScheme {

    KirraArrayList<Triangle> create(float filled, Colorer colorer, TileGenerator tileGenerator, float margin);                  // initial creation of mesh
    KirraArrayList<Triangle> update(float x, float y);  // subsequent update of mesh, given the camera location

}
