package com.tuxware.mesh;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Mesh;
import com.badlogic.gdx.graphics.PerspectiveCamera;
import com.badlogic.gdx.graphics.VertexAttribute;
import com.badlogic.gdx.graphics.VertexAttributes;
import com.badlogic.gdx.graphics.glutils.ShaderProgram;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.GdxRuntimeException;

public class DualLayerRendererImmediate extends TriangleRenderer {

    private static final int NUMBER_OF_POSITION_COMPONENTS = 3;	// xyz
    private static final int NUMBER_OF_COLOR_COMPONENTS = 3;		// rgba
    private static final int NUMBER_OF_NORMAL_COMPONENTS = 3;	// xyz
    private static final int NUMBER_OF_BARY_COMPONENTS = 3;

    private static final int NUM_COMPONENTS = NUMBER_OF_POSITION_COMPONENTS + NUMBER_OF_COLOR_COMPONENTS + NUMBER_OF_NORMAL_COMPONENTS + NUMBER_OF_BARY_COMPONENTS;

    private static final int MAX_TRIS = 5460;
    private static final int MAX_VERTS = MAX_TRIS * 3;

    protected static float[] verts;
    protected static int index = 0;

    protected  Mesh mesh;


    @Override
    public void render(KirraArrayList<Triangle> triangles) {

        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        for (int i = 0; i < triangles.size(); i++) {
            Triangle t1 = triangles.get(i);
            drawTriangle(t1);
        }

        flush();
    }

    private  void drawTriangle(Triangle triangle) {
        if (index ==verts.length)
            flush();

        // vertex culling
        if(cam.frustum.sphereInFrustumWithoutNearFar(triangle.p3, 3000) == false) {
            return;
        }

        Vector3 triangleNormal = triangle.n1;

        float r = triangle.color.r;
        float g = triangle.color.g;
        float b = triangle.color.b;
//        float a = triangle.color.a;


        //bottom left vertex
        verts[index++] = triangle.p1.x; 			//Position(x, y)
        verts[index++] = triangle.p1.y;
        verts[index++] = triangle.p1.z;

        verts[index++] = r; 	//Color(r, g, b, a)
        verts[index++] = g;
        verts[index++] = b;
        //    vboVerts[vboIndex++] = a;

        verts[index++] = triangleNormal.x;
        verts[index++] = triangleNormal.y;
        verts[index++] = triangleNormal.z;

        // barycentric
        verts[index++] = 1.0f;
        verts[index++] = 0.0f;
        verts[index++] = 0.0f;

        //top left vertex
        verts[index++] = triangle.p2.x; 			//Position(x, y)
        verts[index++] = triangle.p2.y;
        verts[index++] = triangle.p2.z;

        verts[index++] = r; 	//Color(r, g, b, a)
        verts[index++] = g;
        verts[index++] = b;
//        vboVerts[vboIndex++] = a;

        verts[index++] = triangleNormal.x;
        verts[index++] = triangleNormal.y;
        verts[index++] = triangleNormal.z;

        // barycentric
        verts[index++] = 0.0f;
        verts[index++] = 1.0f;
        verts[index++] = 0.0f;

        //bottom right vertex
        verts[index++] = triangle.p3.x;	 //Position(x, y)
        verts[index++] = triangle.p3.y;
        verts[index++] = triangle.p3.z;

        verts[index++] = r; 	//Color(r, g, b, a)
        verts[index++] = g;
        verts[index++] = b;
        //      vboVerts[vboIndex++] = a;

        verts[index++] = triangleNormal.x;
        verts[index++] = triangleNormal.y;
        verts[index++] = triangleNormal.z;

        // barycentric
        verts[index++] = 0.0f;
        verts[index++] = 0.0f;
        verts[index++] = 1.0f;
    }


    private  void flush() {

        //if we've already flushed
        if (index == 0) {
            return;
        }

        mesh.setVertices(verts);


        //number of vertices we need to render
        int vertexCount = index / NUM_COMPONENTS;

        //start the texturedTriangleShaderProgram before setting any uniforms
        shaderProgram.begin();

        shaderProgram.setUniform3fv("LightPos",lightPos,0,3);

        //update the projection matrix so our triangles are rendered in 2D
        shaderProgram.setUniformMatrix("u_projTrans", cam.combined);

        //render the mesh
        mesh.render(shaderProgram, GL20.GL_TRIANGLES, 0, vertexCount);

        shaderProgram.end();

        //reset vboIndex to zero
        index = 0;
    }


    public DualLayerRendererImmediate(PerspectiveCamera cam) {

        this.cam = cam;

        mesh = new Mesh(
                true,
                MAX_VERTS,
                0,
                new VertexAttribute(VertexAttributes.Usage.Position, NUMBER_OF_POSITION_COMPONENTS, "a_position"),
                new VertexAttribute(VertexAttributes.Usage.ColorUnpacked, NUMBER_OF_COLOR_COMPONENTS, "a_color"),
                new VertexAttribute(VertexAttributes.Usage.Normal, NUMBER_OF_NORMAL_COMPONENTS, "a_normal"),
                new VertexAttribute(VertexAttributes.Usage.Generic, NUMBER_OF_BARY_COMPONENTS, "barycentric")
        );

        verts = new float[MAX_VERTS * NUM_COMPONENTS];

        String vertexShaderString = Gdx.files.internal("vertex-infotech.glsl").readString();
        String fragmentShaderString = Gdx.files.internal("fragment-infotech-dual-layer.glsl").readString();

//        ShaderProgram.pedantic = true;
        ShaderProgram.pedantic = false;
        shaderProgram = new ShaderProgram(vertexShaderString,fragmentShaderString);

        String log = shaderProgram.getLog();
        if (shaderProgram.isCompiled() == false)
            throw new GdxRuntimeException(log);

        if (log != null)
            System.out.println("Shader Log: " + log);

    }


}
