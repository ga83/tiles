package com.tuxware.mesh;

public interface HeightMap {

    void generate();

    double[][] getHeightMap();

}
