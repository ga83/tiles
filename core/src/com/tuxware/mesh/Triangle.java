package com.tuxware.mesh;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;

public class Triangle {

    public Vector3 p1;
    public Vector3 p2;
    public Vector3 p3;
    public Color color;
    public Vector3 n1;
    public Vector3 n2;
    public Vector3 n3;
    public Texture texture; // only used in schemes that have textures, eg collages
    public boolean textured;
    public Vector2 t1;
    public Vector2 t2;
    public Vector2 t3;


    public Triangle(Triangle triangle) {
        this.p1 = triangle.p1.cpy();
        this.p2 = triangle.p2.cpy();
        this.p3 = triangle.p3.cpy();

        if (triangle.color != null) {
            this.color = triangle.color.cpy();
        }

        if(triangle.n1 != null) {
            this.n1 = triangle.n1.cpy();
        }

        if(triangle.n2 != null) {
            this.n2 = triangle.n2.cpy();
        }

        if(triangle.n3 != null) {
            this.n3 = triangle.n3.cpy();
        }

        this.texture = triangle.texture;
        this.textured = triangle.textured;
    }


    /**
     * Normal is calculated based on vertex positions, so this triangle will probably be rendered with flat shading.
     */
    public Triangle(Vector3 v1, Vector3 v2, Vector3 v3) {
        this.p1 = v1;
        this.p2 = v2;
        this.p3 = v3;
        this.n1 = Utilities.getNormal(this, null);

        // this is never used yet, so leave it out to save battery
        //this.normal = Utilities.getNormal(this, null);
    }

    /**
     * Normal is calculated based on vertex positions, so this triangle will probably be rendered with flat shading.
     */
    public Triangle(Vector3 v1, Vector3 v2, Vector3 v3, Color color) {
        this.p1 = v1;
        this.p2 = v2;
        this.p3 = v3;
        this.color = color;
        this.n1 = Utilities.getNormal(this, null);
        this.n2 = n1;
        this.n3 = n1;
    }

    /**
     * Normal is calculated based on vertex positions, so this triangle will probably be rendered with flat shading.
     */
    public Triangle(Vector3 v1, Vector3 v2, Vector3 v3, Color color, Vector3 outward) {
        this(v1,v2,v3,color);

        // this overwrites the one in the other constructur, could be improved by bypassing the other one
        this.n1 = Utilities.getNormal(this, outward);
        this.n2 = n1;
        this.n3 = n1;
    }

    /**
     * Normal is calculated based on vertex positions, so this triangle will probably be rendered with flat shading.
     */
    public Triangle(Vector3 v1, Vector3 v2, Vector3 v3, Color color, Vector3 outward, boolean textured) {
        this(v1,v2,v3,color);

        // this overwrites the one in the other constructur, could be improved by bypassing the other one
        this.n1 = Utilities.getNormal(this, outward);
        this.n2 = n1;
        this.n3 = n1;
        this.textured = textured;
    }


    public Triangle(Vector3 v1, Vector3 v2, Vector3 v3, Color color, Texture texture) {
        this(v1,v2,v3,color);
        this.texture = texture;
    }

    public Triangle(Vector3 v1, Vector3 v2, Vector3 v3, Color color, Texture texture, boolean textured) {
        this(v1,v2,v3,color);
        this.texture = texture;
        this.textured = textured;
    }

    public Triangle(Vector3 v1, Vector3 v2, Vector3 v3, boolean textured) {
        this(v1,v2,v3);
        this.textured = textured;
    }


    /**
     * All normals are supplied, which means this triangle will probably be rendered with smooth shading.
     */
    public Triangle(Vector3 p1, Vector3 p2, Vector3 p3, Vector3 n1, Vector3 n2, Vector3 n3, Vector2 t1, Vector2 t2, Vector2 t3) {
        this.p1 = p1;
        this.p2 = p2;
        this.p3 = p3;
        this.n1 = n1;
        this.n2 = n2;
        this.n3 = n3;
        this.t1 = t1;
        this.t2 = t2;
        this.t3 = t3;

        System.out.println("===== " +
                        this.p1 + ", " +
                        this.p2 + ", " +
                        this.p3 + ", " +

                        this.n1 + ", " +
                        this.n2 + ", " +
                        this.n3 + ", " +

                        this.t1 + ", " +
                        this.t2 + ", " +
                        this.t3 + ", "
                );
    }
}
