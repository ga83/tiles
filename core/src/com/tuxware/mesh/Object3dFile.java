package com.tuxware.mesh;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;

public class Object3dFile {

    private static final int MAX_POINTS = 100000;
    private KirraArrayList<Vector3> vertexPositions;
    private KirraArrayList<Vector3> vertexNormals;
    private KirraArrayList<Vector2> vertexTextureCoordinates;
    private KirraArrayList<KirraFace> faces;


    public Object3dFile(String objFileName) {
        String objFileString = Gdx.files.internal(objFileName).readString();

        System.out.println(objFileString);

        this.parseFile(objFileString);
    }

    public KirraArrayList<Triangle> getTriangles() {
        System.out.println("===== getTriangles started");

        KirraArrayList<Triangle> triangles = new KirraArrayList<Triangle>(this.faces.size() * 2);   // TODO: each face turns into 2 triangles, at least for now.

        for(int i = 0; i < this.faces.size(); i++) {
            KirraFace face = this.faces.get(i);

            Vector3 p1 = this.vertexPositions.get(face.vertexPositionIndices.get(0) - 1);
            Vector3 p2 = this.vertexPositions.get(face.vertexPositionIndices.get(1) - 1);
            Vector3 p3 = this.vertexPositions.get(face.vertexPositionIndices.get(2) - 1);
         //   Vector3 p4 = this.vertexPositions.get(face.vertexPositionIndices.get(3) - 1);

            Vector3 n1 = this.vertexNormals.get(face.vertexNormalIndices.get(0) - 1);
            Vector3 n2 = this.vertexNormals.get(face.vertexNormalIndices.get(1) - 1);
            Vector3 n3 = this.vertexNormals.get(face.vertexNormalIndices.get(2) - 1);
        //    Vector3 n4 = this.vertexNormals.get(face.vertexNormalIndices.get(3) - 1);

            Vector2 t1 = this.vertexTextureCoordinates.get(face.vertexTextureCoordinateIndices.get(0) - 1);
            Vector2 t2 = this.vertexTextureCoordinates.get(face.vertexTextureCoordinateIndices.get(1) - 1);
            Vector2 t3 = this.vertexTextureCoordinates.get(face.vertexTextureCoordinateIndices.get(2) - 1);
        //    Vector2 t4 = this.vertexTextureCoordinates.get(face.vertexTextureCoordinateIndices.get(3) - 1);

            Triangle triangle1 = new Triangle(p1, p2, p3, n1, n2, n3, t1, t2, t3);
         //   Triangle triangle2 = new Triangle(p1, p3, p4, n1, n3, n4, t1, t3, t4);

            triangles.add(triangle1);
          //  triangles.add(triangle2);
        }

        System.out.println("===== getTriangles ending");
        return triangles;
    }

    private void parseFile(String objFileString) {
        String[] objFileLines = objFileString.split(System.lineSeparator());

        this.vertexPositions = new KirraArrayList<Vector3>(MAX_POINTS);
        this.vertexNormals = new KirraArrayList<Vector3>(MAX_POINTS);
        this.vertexTextureCoordinates = new KirraArrayList<Vector2>(MAX_POINTS);
        this.faces = new KirraArrayList<KirraFace>(MAX_POINTS / 4);

        for(int i = 0; i < objFileLines.length; i++) {
            System.out.println("===== line: " + i);

            String line = objFileLines[i];
            String[] lineTokens = line.split(" ");

            // process vertex position
            if(lineTokens[0].equals("v") == true) {
                float f1 = Float.parseFloat(lineTokens[1]);
                float f2 = Float.parseFloat(lineTokens[2]);
                float f3 = Float.parseFloat(lineTokens[3]);
                Vector3 vertexPosition = new Vector3(f1, f2, f3);

                System.out.println("===== adding vertex position " + vertexPosition);
                this.vertexPositions.add(vertexPosition);
            }

            // process vertex normal
            else if(lineTokens[0].equals("vn") == true) {
                float f1 = Float.parseFloat(lineTokens[1]);
                float f2 = Float.parseFloat(lineTokens[2]);
                float f3 = Float.parseFloat(lineTokens[3]);
                Vector3 vertexNormal = new Vector3(f1, f2, f3);

                System.out.println("===== adding vertex normal " + vertexNormal);
                this.vertexNormals.add(vertexNormal);
            }

            // process vertex texture coordinates
            else if(lineTokens[0].equals("vt") == true) {
                float f1 = Float.parseFloat(lineTokens[1]);
                float f2 = Float.parseFloat(lineTokens[2]);
                Vector2 vertexTextureCoordinate = new Vector2(f1, f2);

                System.out.println("===== adding vertex texture coordinate " + vertexTextureCoordinate);
                this.vertexTextureCoordinates.add(vertexTextureCoordinate);
            }

            // process face
            else if(lineTokens[0].equals("f") == true) {
                // TODO: assume the faces are always 3 sided triangles. we can use triangulate mesh in blender to force this.
                try {
                    if (lineTokens.length != 1 + 3) {
                        throw new Exception("Face contains other than 4 vertices");
                    }

                    // positions
                    int p1 = Integer.parseInt(lineTokens[1].split("/")[0]);
                    int p2 = Integer.parseInt(lineTokens[2].split("/")[0]);
                    int p3 = Integer.parseInt(lineTokens[3].split("/")[0]);
                 //   int p4 = Integer.parseInt(lineTokens[4].split("/")[0]);

                    // texture coordinates
                    int t1 = Integer.parseInt(lineTokens[1].split("/")[1]);
                    int t2 = Integer.parseInt(lineTokens[2].split("/")[1]);
                    int t3 = Integer.parseInt(lineTokens[3].split("/")[1]);
                 //   int t4 = Integer.parseInt(lineTokens[4].split("/")[1]);

                    // normals
                    int n1 = Integer.parseInt(lineTokens[1].split("/")[2]);
                    int n2 = Integer.parseInt(lineTokens[2].split("/")[2]);
                    int n3 = Integer.parseInt(lineTokens[3].split("/")[2]);
                 //   int n4 = Integer.parseInt(lineTokens[4].split("/")[2]);

                    KirraArrayList<Integer> vertexPositionIndices = new KirraArrayList<Integer>(4);
                    vertexPositionIndices.add(p1);
                    vertexPositionIndices.add(p2);
                    vertexPositionIndices.add(p3);
                 //   vertexPositionIndices.add(p4);

                    KirraArrayList<Integer> vertexNormalIndices = new KirraArrayList<Integer>(4);
                    vertexNormalIndices.add(n1);
                    vertexNormalIndices.add(n2);
                    vertexNormalIndices.add(n3);
                  //  vertexNormalIndices.add(n4);

                    KirraArrayList<Integer> vertexTextureCoordinateIndices = new KirraArrayList<Integer>(4);
                    vertexTextureCoordinateIndices.add(t1);
                    vertexTextureCoordinateIndices.add(t2);
                    vertexTextureCoordinateIndices.add(t3);
                 //   vertexTextureCoordinateIndices.add(t4);

                    KirraFace face = new KirraFace(vertexPositionIndices, vertexNormalIndices, vertexTextureCoordinateIndices);

                    System.out.println("===== adding face " + face + ", on line " + i);
                    this.faces.add(face);
                }
                catch(Exception e) {
                    System.out.println(e.getMessage());
                    //System.exit(1);
                    continue;
                }
            }
        }

    }
}
