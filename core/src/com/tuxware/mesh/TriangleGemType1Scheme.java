package com.tuxware.mesh;

import com.badlogic.gdx.math.Vector3;

import java.util.ArrayList;

public class TriangleGemType1Scheme implements MeshScheme {

    protected KirraArrayList<Triangle> triangles = TriangleListSingleton.triangles;

    private static float SIDE_LENGTH = 1000.0f;
    private static double ROW_HEIGHT = Math.sin(Math.toRadians(60.0)) * SIDE_LENGTH;   // sin 60 * side length
    private static float RADIUS = (float) (SIDE_LENGTH / Math.sqrt(3.0));
    private static double CENTRE_VERTICAL_OFFSET = (ROW_HEIGHT - RADIUS) / 2;
    private static float COLUMN_WIDTH_ADJUSTED = SIDE_LENGTH / 2;

    private static Vector3 ROTATION_AXIS = new Vector3(0.0f, 0.0f, 1.0f);
    private static Vector3 DUMMY_CENTRE = new Vector3(0.0f, 0.0f, 0.0f);



    @Override
    public KirraArrayList<Triangle> create(float filled, Colorer colorer, TileGenerator tileGenerator, float margin) {

        triangles.clear();

        for(int row = -26; row < 26; row++) {
            for (int column = -26; column < 26; column++) {

                boolean flipTriangle = column % 2 == 0 ^ row % 2 == 0;
                float sign = flipTriangle ? 1.0f : -1.0f;

                float xCentre = column * COLUMN_WIDTH_ADJUSTED;
                float yCentre = (float) (row * ROW_HEIGHT + CENTRE_VERTICAL_OFFSET * sign);

                this.generateTriangle(xCentre, yCentre, colorer, filled, flipTriangle, column, row, tileGenerator, margin);
            }
        }


        return triangles;
    }

    @Override
    public KirraArrayList<Triangle> update(float x, float y) {
        return triangles;
    }

    protected void generateTriangle(float xcentre, float ycentre, Colorer colorer, float filled, boolean flip, int column, int row, TileGenerator tileGenerator, float margin) {
        float[] colorComponents;

        if(MeshWallpaper.rnd.nextFloat() < filled) {
            colorComponents = colorer.getColor(column, row);
        } else {
            colorComponents = Color.NONFILLED;
        }

        float radius = RADIUS;

        Vector3 dummyCentre = DUMMY_CENTRE; // we are passing in a dummy centre for tile generation because we want to rotate it, and rotation must come before translation.
        com.badlogic.gdx.graphics.Color color = new com.badlogic.gdx.graphics.Color(colorComponents[0], colorComponents[1], colorComponents[2], 1.0f);
        KirraArrayList<Triangle> triangles = tileGenerator.generateTile(radius, dummyCentre, 3, color, RotateExtra.PLUS_NINETY, margin);


        if(flip == true) {
            GeometryOperations.rotateAllTriangles(triangles, 180.0f, ROTATION_AXIS);
        }

        GeometryOperations.translateAllTriangles(triangles, xcentre, ycentre, 0.0f);

        this.triangles.addAll(triangles);
    }

}















