package com.tuxware.mesh;

/**
 * Created by Guy Aaltonen on 5/09/18.
 */
public interface AppUsageLogger  {

    void log() throws ParametersNotSetException;
    void setUrl(String url);
    void setApplicationName(String appname);
}
