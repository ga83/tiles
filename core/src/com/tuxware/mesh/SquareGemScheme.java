package com.tuxware.mesh;

import com.badlogic.gdx.math.Vector3;

public class SquareGemScheme implements MeshScheme {

    private static KirraArrayList<Triangle> triangles = TriangleListSingleton.triangles;

    private static final float Z_COORDINATE_BASE    = 1070.0f;
    private static int SIDE_LENGTH = 1000;


    @Override
    public KirraArrayList<Triangle> create(float filled, Colorer colorer, TileGenerator tileGenerator, float margin) {

        triangles.clear();

/*
        for(int column = -26; column < 26; column++) {
            for (int row = -26; row < 26; row++) {
                generateSquare(SIDE_LENGTH * column, SIDE_LENGTH * row, colorer, filled, column, row, tileGenerator, margin);
            }
        }
*/
        for(int column = -16; column < 16; column++) {
            for (int row = -16; row < 16; row++) {
                generateSquare(SIDE_LENGTH * column, SIDE_LENGTH * row, colorer, filled, column, row, tileGenerator, margin);
            }
        }

        return triangles;
    }


    @Override
    public KirraArrayList<Triangle> update(float x, float y) {

        // incomplete

        return triangles;
    }

    private void generateSquare(float xcentre, float ycentre, Colorer colorer, float filled, int column, int row, TileGenerator tileGenerator, float margin) {

        // base vertices
        Vector3 baseTopLeft     = new Vector3(-SIDE_LENGTH / 2 + xcentre + margin,  SIDE_LENGTH / 2 + ycentre - margin, Z_COORDINATE_BASE);
        Vector3 baseBottomRight = new Vector3( SIDE_LENGTH / 2 + xcentre - margin, -SIDE_LENGTH / 2 + ycentre + margin, Z_COORDINATE_BASE);


        float[] colorComponents;

        if(MeshWallpaper.rnd.nextFloat() < filled) {
            colorComponents = colorer.getColor(column, row);
        } else {
            colorComponents = Color.NONFILLED;
        }

        float radius = baseBottomRight.dst(baseTopLeft) / 2.0f;
        Vector3 centre = new Vector3(xcentre, ycentre, 0);
        com.badlogic.gdx.graphics.Color color = new com.badlogic.gdx.graphics.Color(colorComponents[0], colorComponents[1], colorComponents[2], 1f);

        System.out.println("===== calling generatetile");
        KirraArrayList<Triangle> triangles = tileGenerator.generateTile(radius, centre, 4, color, RotateExtra.HALF, margin);

        this.triangles.addAll(triangles);

    }
}










