package com.tuxware.mesh;

class RandomColorer extends Colorer {

    @Override
    public float[] getColor(float x, float y) {

        float[] colorComponents;

        colorComponents = Color.getRandomColorFromScheme(colorScheme, false);

        return colorComponents;
    }

    public RandomColorer(String colorScheme) {
        super(colorScheme);
    }



}
