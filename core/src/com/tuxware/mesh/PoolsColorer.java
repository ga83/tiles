package com.tuxware.mesh;

public class PoolsColorer extends Colorer {

    public PoolsColorer(String colorScheme) {
        super(colorScheme);

        heightMap = new PoolsHeightMap(26 * 2, 26 * 2);
        heightMap.generate();

        for(int i = 0; i < PoolsHeightMap.NUM_COLORS; i++) {
            colorComponentsArray[i] = Color.getRandomColorFromScheme(colorScheme, false);
        }

    }


    public float[] getColor(float x, float y) {
        int height = (int) heightMap.getHeightMap()[26 + (int)x][26 + (int)y];
        float[] colorComponents = colorComponentsArray[height];
        return colorComponents;
    }

}
