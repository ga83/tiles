package com.tuxware.mesh;

import com.badlogic.gdx.Gdx;

public class FpsLimiter15 implements FpsLimiter {

    public void limitFps() {
            try {
                Thread.sleep(MeshWallpaper.sleepDuration);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        if (Gdx.graphics.getRawDeltaTime() > 1.0f / 14.0f) {
            MeshWallpaper.sleepDuration -= 1;

            if (MeshWallpaper.sleepDuration < 0){
                MeshWallpaper.sleepDuration = 0;
            }

        } else if (Gdx.graphics.getRawDeltaTime() < 1.0f / 16.0f) {
            MeshWallpaper.sleepDuration += 1;
        }
    }

}
