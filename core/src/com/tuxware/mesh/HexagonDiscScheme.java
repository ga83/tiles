package com.tuxware.mesh;

import com.badlogic.gdx.math.Vector3;

import java.util.ArrayList;

/**
 * Multi-layer hexagon / honeycomb pattern like KDE wallpaper has.
 */

//TODO: must implement some type of culling for faces, both for backward facing ones, and ones nowhere near on screen. very battery intensive at the moment.
public class HexagonDiscScheme implements MeshScheme {

    private static KirraArrayList<Triangle> triangles = TriangleListSingleton.triangles;

    public static final int PERLIN_VALUE_MULTIPLIER = 4;
    public static final int PERLIN_PERIOD = 6000;

    private static final float Z_COORDINATE_BASE    = 1200.0f;
    private static final float Z_COORDINATE_TOP = 1000.0f;
    private static final float Z_DISC_HEIGHT = Z_COORDINATE_BASE - Z_COORDINATE_TOP;

    private static final float HEXAGON_CIRCUMCIRCLE_RADIUS_OR_SIDE_LENGTH = 1000.0f / 1;     // from the centre to one of the vertices
    private static final float HEXAGON_INCIRCLE_RADIUS = 866.0f / 1; // not right, just guessing


    @Override
    public KirraArrayList<Triangle> create(float filled, Colorer colorer, TileGenerator tileGenerator, float margin) {

        triangles.clear();

        float perlinOffsetX = MeshWallpaper.rnd.nextFloat() * 100;
        float perlinOffsetY = MeshWallpaper.rnd.nextFloat() * 100;


        for(int column = -26; column < 26; column++) {
            for (int row = -26; row < 26; row++) {

                float widthTakenPerHexagon = (float) ((HEXAGON_CIRCUMCIRCLE_RADIUS_OR_SIDE_LENGTH * 2) - Math.cos(Math.toRadians(60.0)) * HEXAGON_CIRCUMCIRCLE_RADIUS_OR_SIDE_LENGTH);

                float verticalPadding = column % 2 == 0 ? HEXAGON_INCIRCLE_RADIUS: 0;
                float xcentre = column * widthTakenPerHexagon;
                float ycentre = row * HEXAGON_INCIRCLE_RADIUS * 2 + verticalPadding;

                // get height from perlin noise
                double px;
                double py;

                px = xcentre / PERLIN_PERIOD;
                py = ycentre / PERLIN_PERIOD;

                px += perlinOffsetX;
                py += perlinOffsetY;

                float perlinValueFloat = (float) (ImprovedNoise.noise(px, py, 0.0));

                // add 1 before we multiply to make sure we have no negatives
                perlinValueFloat += 1.0f;

                int totalDiscHeight = (int)(perlinValueFloat * PERLIN_VALUE_MULTIPLIER) - 3;
                totalDiscHeight = Math.max(totalDiscHeight,0);

                for(int currentDiscHeight = 0; currentDiscHeight < totalDiscHeight; currentDiscHeight++) {
                    generateHexagonDisc(xcentre, ycentre, currentDiscHeight, totalDiscHeight, colorer, filled, column, row);
                }
            }
        }

        return triangles;
    }

    private void generateHexagonDisc(float xcentre, float ycentre, int discHeight, int totalDiscHeight, Colorer colorer, float filled, int column, int row) {

        float baseZ = Z_DISC_HEIGHT * -discHeight;

        float[] colorComponents;

        if(MeshWallpaper.rnd.nextFloat() < filled) {
            colorComponents = colorer.getColor(column, row);
        } else {
            colorComponents = Color.NONFILLED;
        }

        com.badlogic.gdx.graphics.Color color = new com.badlogic.gdx.graphics.Color(colorComponents[0], colorComponents[1], colorComponents[2], 1f);

        float margin = 10;

        for(int i = 0; i < 6; i++) {

            double leftArmAngle  = (360.0 / 6) * i;
            double rightArmAngle = (360.0 / 6) * (i + 1);

            // base vertices
            double baseLeftArmX = Math.cos(Math.toRadians(leftArmAngle)) * (HEXAGON_CIRCUMCIRCLE_RADIUS_OR_SIDE_LENGTH - margin);
            double baseLeftArmY = Math.sin(Math.toRadians(leftArmAngle)) * (HEXAGON_CIRCUMCIRCLE_RADIUS_OR_SIDE_LENGTH - margin);
            double baseRightArmX = Math.cos(Math.toRadians(rightArmAngle)) * (HEXAGON_CIRCUMCIRCLE_RADIUS_OR_SIDE_LENGTH - margin);
            double baseRightArmY = Math.sin(Math.toRadians(rightArmAngle)) * (HEXAGON_CIRCUMCIRCLE_RADIUS_OR_SIDE_LENGTH - margin);
            Vector3 baseLeft  = new Vector3(xcentre + (float)baseLeftArmX, ycentre + (float)baseLeftArmY, Z_COORDINATE_BASE + baseZ);
            Vector3 baseRight = new Vector3(xcentre + (float)baseRightArmX, ycentre + (float)baseRightArmY, Z_COORDINATE_BASE + baseZ);

            // top vertices
            Vector3 roofCentre = new Vector3(xcentre, ycentre, Z_COORDINATE_TOP + baseZ);
            Vector3 roofLeftVertex  = baseLeft.cpy(); roofLeftVertex.z = Z_COORDINATE_TOP  + baseZ;
            Vector3 roofRightVertex = baseRight.cpy(); roofRightVertex.z = Z_COORDINATE_TOP  + baseZ;

            Triangle triangle;

            // wall triangle 1
            triangle = new Triangle(baseLeft, roofRightVertex, roofLeftVertex, color, baseLeft.cpy().sub(roofCentre));
            triangles.add(triangle);

            // wall triangle 2
            triangle = new Triangle(baseRight, roofRightVertex, baseLeft, color, baseRight.cpy().sub(roofCentre));
            triangles.add(triangle);

            // only draw the top if it's the top disc of the stack
            if(discHeight == totalDiscHeight - 1 && totalDiscHeight > 0) {
                // top triangle
                triangle = new Triangle(roofRightVertex, roofLeftVertex, roofCentre, color);
                triangles.add(triangle);
            }
        }

    }

    @Override
    public KirraArrayList<Triangle> update(float x, float y) {
        return null;
    }
}
