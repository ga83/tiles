package com.tuxware.mesh;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.loaders.ModelLoader;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g3d.Model;
import com.badlogic.gdx.graphics.g3d.ModelInstance;
import com.badlogic.gdx.graphics.g3d.loader.ObjLoader;
import com.badlogic.gdx.math.Vector3;

public class ObjectTileGenerator extends TileGenerator {

    private static final float Z_COORDINATE_BASE = 1070.0f;
    private final KirraArrayList<Triangle> SMOOTH_TILE;


    public ObjectTileGenerator() {
        // String objFileName = "tile-simple-01.obj";
        // String objFileName = "tile-04.obj";
         String objFileName = "tile-05-triangulated.obj";
        // String objFileName = "tile-simple-02-triangulated.obj";
        // String objFileName = "cube-01-triangulated.obj";
        // String objFileName = "cube-02-triangulated.obj";
        // String objFileName = "ico-01-triangulated.obj";
         // String objFileName = "sharp-01-triangulated.obj";
        // String objFileName = "mound-01-triangulated.obj";
        SMOOTH_TILE = createObjTileGeometry(objFileName);
    }

    @Override
    public KirraArrayList<Triangle> generateTile(float radius, Vector3 centre, int numSides, Color color, RotateExtra rotateExtra, float margin) {
        System.out.println("===== generateTile " + centre + ", " + radius);
        KirraArrayList<Triangle> tile = GeometryOperations.copyTriangles(SMOOTH_TILE, color);

       // System.out.println("===== tile posi bef: " + tile.get(0).v1 + ", " + tile.get(0).v2 + ", " + tile.get(0).v3);

        GeometryOperations.scaleAllTriangles(tile, radius * 1);
        GeometryOperations.translateAllTriangles(tile, centre.x, centre.y, Z_COORDINATE_BASE);
        GeometryOperations.translateAllTriangles(tile, 0,0, Z_COORDINATE_BASE);

        //System.out.println("===== tile posi aft: " + tile.get(0).v1 + ", " + tile.get(0).v2 + ", " + tile.get(0).v3);

        return tile;
    }

    private KirraArrayList<Triangle> createObjTileGeometry(String objFileName) {
        System.out.println("===== createObjTileGeometry starting");
        //KirraArrayList<Triangle> triangles = new KirraArrayList<Triangle>(10000);   // TODO: reduce size.

        Object3dFile object3dFile = new Object3dFile(objFileName);

        System.out.println("===== createObjTileGeometry ending");

        return object3dFile.getTriangles();
    }

}
