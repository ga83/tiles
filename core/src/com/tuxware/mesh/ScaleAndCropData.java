package com.tuxware.mesh;

public class ScaleAndCropData {

    public int scale;
    public int x;
    public int y;
    public int width;
    public int height;

}
