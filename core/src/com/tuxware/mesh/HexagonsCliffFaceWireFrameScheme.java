package com.tuxware.mesh;

import com.badlogic.gdx.math.Vector3;

import java.util.ArrayList;


/**
 * This is a vertical-plane mesh, just like most, eg hexagons and diamonds. Low resolution mesh. Meant to be viewed close up. CONTRAST to HexagonsLandscapeScheme.
 */
public class HexagonsCliffFaceWireFrameScheme implements MeshScheme {

    public static final int PERLIN_VALUE_MULTIPLIER = 3400;
    public static final int PERLIN_VALUE_MULTIPLIER_LAYER_2 = 3400;

    public static final int PERLIN_PERIOD = 5000;
    public static final int PERLIN_PERIOD_LAYER_2 = 5000;

    private static KirraArrayList<Triangle> triangles = TriangleListSingleton.triangles;

    private static final float Z_COORDINATE_BASE = 2000.0f;
    private static final float Z_COORDINATE_BASE_LAYER_2 = 2050.0f;
    private static final float Z_COORDINATE_BASE_LAYER_3 = 2100.0f;
    private static final float Z_COORDINATE_BASE_LAYER_4 = 2150.0f;

    private static final float HEXAGON_CIRCUMCIRCLE_RADIUS_OR_SIDE_LENGTH = 1000.0f * 1.5f;     // from the centre to one of the vertices
    private static final float HEXAGON_INCIRCLE_RADIUS = 866.0f * 1.5f; // not right, just guessing

    @Override
    public KirraArrayList<Triangle> create(float filled, Colorer colorer, TileGenerator tileGenerator, float margin) {

        triangles.clear();

        float perlinOffsetX;
        float perlinOffsetY;

        perlinOffsetX = MeshWallpaper.rnd.nextFloat() * 100;
        perlinOffsetY = MeshWallpaper.rnd.nextFloat() * 100;

        // layer 1
        generateLayer(filled, colorer, Z_COORDINATE_BASE, PERLIN_VALUE_MULTIPLIER, PERLIN_PERIOD, perlinOffsetX, perlinOffsetY);

        return triangles;
    }


    private void generateLayer(float filled, Colorer colorer, float zBase, float perlinMultiplier, float perlinPeriod, float perlinOffsetX, float perlinOffsetY) {
        for(int column = -26; column < 26; column++) {
            for (int row = -26; row < 26; row++) {

                float widthTakenPerHexagon = (float) ((HEXAGON_CIRCUMCIRCLE_RADIUS_OR_SIDE_LENGTH * 2) - Math.cos(Math.toRadians(60.0)) * HEXAGON_CIRCUMCIRCLE_RADIUS_OR_SIDE_LENGTH);

                float verticalPadding = column % 2 == 0 ? HEXAGON_INCIRCLE_RADIUS: 0;
                float xcentre = column * widthTakenPerHexagon;
                float ycentre = row * HEXAGON_INCIRCLE_RADIUS * 2 + verticalPadding;

                generateHexagon(xcentre, ycentre, colorer, filled, column, row, zBase, perlinMultiplier, perlinPeriod, perlinOffsetX, perlinOffsetY);
            }
        }
    }

    private void generateHexagon(float xcentre, float ycentre, Colorer colorer, float filled, int column, int row, float zBase, float perlinMultiplier, float perlinPeriod, float perlinOffsetX, float perlinOffsetY) {
        for(int i = 0; i < 6; i++) {

            float[] colorComponents;
            if(MeshWallpaper.rnd.nextFloat() < filled) {
                colorComponents = colorer.getColor(column, row);
            } else {
                colorComponents = Color.NONFILLED;
            }

            com.badlogic.gdx.graphics.Color color = new com.badlogic.gdx.graphics.Color(colorComponents[0], colorComponents[1], colorComponents[2], 1f);

            double leftArmAngle  = (360.0 / 6) * i;
            double rightArmAngle = (360.0 / 6) * (i + 1);

            double leftArmX = Math.cos(Math.toRadians(leftArmAngle)) * HEXAGON_CIRCUMCIRCLE_RADIUS_OR_SIDE_LENGTH;
            double leftArmY = Math.sin(Math.toRadians(leftArmAngle)) * HEXAGON_CIRCUMCIRCLE_RADIUS_OR_SIDE_LENGTH;

            double rightArmX = Math.cos(Math.toRadians(rightArmAngle)) * HEXAGON_CIRCUMCIRCLE_RADIUS_OR_SIDE_LENGTH;
            double rightArmY = Math.sin(Math.toRadians(rightArmAngle)) * HEXAGON_CIRCUMCIRCLE_RADIUS_OR_SIDE_LENGTH;

            // TODO: let the user choose the level of deformation maybe. right now, we're using a hardcoded value for PERLIN_VALUE_MULTIPLIER. user might not even want any noise,
            // TODO: and instead only want the centre point closer to the camera, for example.
            // TODO: if the light source is moving around, it could look very good with perlin noise!!! crevases, usually dark, can light up.
            float perlinCentre     = (float) ImprovedNoise.noise(xcentre / perlinPeriod + perlinOffsetX, ycentre / perlinPeriod + perlinOffsetY, 0.0) * perlinMultiplier;
            float perlinLeftArm    = (float) ImprovedNoise.noise((xcentre + leftArmX) / perlinPeriod + perlinOffsetX, (ycentre + leftArmY) / perlinPeriod + perlinOffsetY, 0.0) * perlinMultiplier;
            float perlinRightArm   = (float) ImprovedNoise.noise((xcentre + rightArmX) / perlinPeriod + perlinOffsetX, (ycentre + rightArmY) / perlinPeriod + perlinOffsetY, 0.0) * perlinMultiplier;

            Vector3 centre      = new Vector3(xcentre, ycentre, zBase + perlinCentre);
            Vector3 leftVertex  = new Vector3(xcentre + (float)leftArmX, ycentre + (float)leftArmY, zBase + perlinLeftArm);
            Vector3 rightVertex = new Vector3(xcentre + (float)rightArmX, ycentre + (float)rightArmY, zBase + perlinRightArm);

            Triangle triangle = new Triangle(centre, leftVertex, rightVertex, color);
            triangles.add(triangle);
        }

    }

    @Override
    public KirraArrayList<Triangle> update(float x, float y) {
        return null;
    }
}
