package com.tuxware.mesh;

public abstract class Colorer {

    protected float[][] colorComponentsArray;
    protected HeightMap heightMap;
    protected String colorScheme;


    // for some colorers, it matters what the x and y are, because they depend on a heightmap.
    // for others (eg random color), it only matters the scheme, so x and y will be ignored
    public abstract float[] getColor(float x, float y);

    public Colorer(String colorScheme) {
        colorComponentsArray = new float[PoolsHeightMap.NUM_COLORS][3];
        this.colorScheme = colorScheme;
    }
}
