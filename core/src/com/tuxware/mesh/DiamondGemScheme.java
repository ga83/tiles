package com.tuxware.mesh;

import com.badlogic.gdx.math.Vector3;

import java.util.ArrayList;

public class DiamondGemScheme implements MeshScheme {

    private static KirraArrayList<Triangle> triangles = TriangleListSingleton.triangles;

    private static final float Z_COORDINATE_BASE    = 1070.0f;
    private static final float Z_COORDINATE_TOP = 1000.0f;

    private static final float TOP_LENGTH_PERCENTAGE = 0.75f; // the proportion of the base's edge lengths to use for the top's edge lengths

    private static int DIAMOND_WIDTH = 1000;
    private static int DIAMOND_HEIGHT = 2000;

    @Override
    public KirraArrayList<Triangle> create(float filled, Colorer colorer, TileGenerator tileGenerator, float margin) {

        triangles.clear();

        // -7 to 8, with this side length, just covers the screen width. might need to generate more if we're going to allow arbitrary scrolling.
        for(int row = -26; row < 26; row++) {
            for (int column = -26; column < 26; column++) {
                int verticalOffset = column % 2 == 0 ? DIAMOND_HEIGHT / 2 : 0;
                generateDiamond((column * DIAMOND_WIDTH) / 2, DIAMOND_HEIGHT * row + verticalOffset, colorer, filled, column, row, margin);
            }
        }
        return triangles;
    }

    @Override
    public KirraArrayList<Triangle> update(float x, float y) {

        // incomplete

        return triangles;
    }

    private void generateDiamond(float xcentre, float ycentre, Colorer colorer, float filled, int column, int row, float margin) {

        // base vertices
        Vector3 baseTop    = new Vector3(xcentre, ycentre + DIAMOND_HEIGHT / 2, Z_COORDINATE_BASE);
        Vector3 baseRight  = new Vector3(xcentre + DIAMOND_WIDTH / 2, ycentre, Z_COORDINATE_BASE);
        Vector3 baseBottom = new Vector3(xcentre, ycentre - DIAMOND_HEIGHT / 2, Z_COORDINATE_BASE);
        Vector3 baseLeft   = new Vector3(xcentre - DIAMOND_WIDTH / 2, ycentre, Z_COORDINATE_BASE);

        // top vertices
        Vector3 roofTop    = new Vector3(xcentre, ycentre + DIAMOND_HEIGHT / 2 * TOP_LENGTH_PERCENTAGE, Z_COORDINATE_TOP);
        Vector3 roofRight  = new Vector3(xcentre + DIAMOND_WIDTH / 2 * TOP_LENGTH_PERCENTAGE, ycentre, Z_COORDINATE_TOP);
        Vector3 roofBottom = new Vector3(xcentre, ycentre - DIAMOND_HEIGHT / 2 * TOP_LENGTH_PERCENTAGE, Z_COORDINATE_TOP);
        Vector3 roofLeft   = new Vector3(xcentre - DIAMOND_WIDTH / 2 * TOP_LENGTH_PERCENTAGE, ycentre, Z_COORDINATE_TOP);


        Triangle triangle;

        float[] colorComponents;

        if(MeshWallpaper.rnd.nextFloat() < filled) {
            colorComponents = colorer.getColor(column, row);
        } else {
            colorComponents = Color.NONFILLED;
        }

        com.badlogic.gdx.graphics.Color color = new com.badlogic.gdx.graphics.Color(colorComponents[0], colorComponents[1], colorComponents[2], 1f);


        // top right
        triangle = new Triangle(baseTop.cpy(), baseRight.cpy(), roofTop.cpy(), color);
        triangles.add(triangle);

        triangle = new Triangle(baseRight.cpy(), roofRight.cpy(), roofTop.cpy(), color);
        triangles.add(triangle);

        // bottom right
        triangle = new Triangle(baseRight.cpy(), baseBottom.cpy(), roofRight.cpy(), color);
        triangles.add(triangle);

        triangle = new Triangle(baseBottom.cpy(), roofBottom.cpy(), roofRight.cpy(), color);
        triangles.add(triangle);

        // bottom left
        triangle = new Triangle(baseBottom.cpy(), baseLeft.cpy(), roofLeft.cpy(), color);
        triangles.add(triangle);

        triangle = new Triangle(roofLeft.cpy(), roofBottom.cpy(), baseBottom.cpy(), color);
        triangles.add(triangle);

        // top left
        triangle = new Triangle(baseLeft.cpy(), baseTop.cpy(), roofTop.cpy(), color);
        triangles.add(triangle);

        triangle = new Triangle(roofTop.cpy(), roofLeft.cpy(), baseLeft.cpy(), color);
        triangles.add(triangle);

        // roof
        triangle = new Triangle(roofTop.cpy(), roofRight.cpy(), roofBottom.cpy(), color);
        triangles.add(triangle);

        triangle = new Triangle(roofBottom.cpy(), roofLeft.cpy(), roofTop.cpy(), color);
        triangles.add(triangle);

    }
}
