package com.tuxware.mesh;

import com.badlogic.gdx.Gdx;

public class FpsLimiter45 implements FpsLimiter {

    public void limitFps() {
        try {
            Thread.sleep(MeshWallpaper.sleepDuration);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        if (Gdx.graphics.getRawDeltaTime() > 1.0f / 44.0f) {
            MeshWallpaper.sleepDuration -= 1;

            if (MeshWallpaper.sleepDuration < 0){
                MeshWallpaper.sleepDuration = 0;
            }

        } else if (Gdx.graphics.getRawDeltaTime() < 1.0f / 46.0f) {
            MeshWallpaper.sleepDuration += 1;
        }
    }

}
