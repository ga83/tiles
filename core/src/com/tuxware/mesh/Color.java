package com.tuxware.mesh;

import java.util.Calendar;
import java.util.Random;

/**
 * Created by Guy Aaltonen on 23/08/17.
 */
public class Color {


    public static final float[] BLACK = new float[] { 0.15f, 0.15f, 0.15f };
    public static final float[] GRAY = new float[] { 0.5f, 0.5f, 0.5f };
    public static final float[] WHITE = new float[] { 1.0f, 1.0f, 1.0f };

    public static float[] NONFILLED = BLACK;


    private static Random rnd = new Random();

    private static String[] SCHEMES_TYPE_2 = new String[] { "time","timetripod","timebipod" };
	private static String[] SCHEMES_TYPE_3 = new String[] { "bluereddiscrete", "redgreenblue", "beach", "goldsilverbronze", "fluro", "all" };
    private static String[] SCHEMES_TYPE_4 = new String[] { "redyellow", "yellowgreen", "greencyan", "cyanblue", "bluemagenta", "magentared" };

    public int r;
    public int g ;
    public int b ;


    public Color(int r,int g,int b) {
        this.r = r;
        this.g = g;
        this.b = b;
    }

    public Color(int r, int g, int b, int a) {
        this.r = r;
        this.g = g;
        this.b = b;
    }

    public static void setNonFilledColor(String color) {
        if(color.equals("white") == true) {
            NONFILLED = WHITE;
        }
        if(color.equals("gray") == true) {
            NONFILLED = GRAY;
        }
        if(color.equals("black") == true) {
            NONFILLED = BLACK;
        }
    }


    public static float[] getColorFromHue(float hue) {

        while (hue < 0.0f) {
            hue += 1.0f;
        }


        while (hue > 1.0f) {
            hue -= 1.0f;
        }

        float saturation = 0.99f;
        float lightness = 0.075f * 0.0025f * 3500f;

        return hslToRgb(hue, saturation, lightness);
    }

    public static float[] getRandomColorFromScheme(String colorParams, boolean returnHsl) {

        float saturation = 0.99f;
        float lightness = 0.5f;

        if(arrayContainsString(SCHEMES_TYPE_2, colorParams)) {
			if(colorParams.equals("time") == true) {
				float hue = getBase();
                hue += rnd.nextFloat() * 0.33f;
				if(hue > 1.0f) { hue -= 1.0f; }
                if(returnHsl) {
                    return new float[] { hue, saturation, lightness };
                } else {
                    return hslToRgb(hue, saturation, lightness);
                }
			}
			if(colorParams.equals("timetripod") == true) {
				float hue = getBase();
                hue += rnd.nextFloat() * 0.05f;
				int random = rnd.nextInt(3);
				if(random == 1) { hue += 0.33f; }
				if(random == 2) { hue += 0.66f; }
				if(hue > 1.0f) { hue -= 1.0f; }
                if(returnHsl) {
                    return new float[] { hue, saturation, lightness };
                } else {
                    return hslToRgb(hue, saturation, lightness);
                }
			}
			if(colorParams.equals("timebipod") == true) {
				float hue = getBase();
                hue += rnd.nextFloat() * 0.05f;
				int random = rnd.nextInt(2);
				if(random == 1) { hue += 0.5f; }
				if(hue > 1.0f) { hue -= 1.0f; }
                if(returnHsl) {
                    return new float[] { hue, saturation, lightness };
                } else {
                    return hslToRgb(hue, saturation, lightness);
                }
			}
			
		}

		else if(arrayContainsString(SCHEMES_TYPE_3, colorParams)) {

            if(colorParams.equals("redgreenblue") == true) {
                float hue;

                int rgb = rnd.nextInt(3);

                if(rgb == 0) {
                    hue = 0.0f;
                }
                else if(rgb == 1) {
                    hue = 0.33f;
                }
                else {
                    hue = 0.66f;
                }
                if(returnHsl) {
                    return new float[] { hue, saturation, lightness };
                } else {
                    return hslToRgb(hue, saturation, lightness);
                }
            }

            else if(colorParams.equals("bluereddiscrete") == true) {
                float hue;

                int rgb = rnd.nextInt(2);

                if(rgb == 0) {
                    hue = 0.66f;
                }
                else {
                    hue = 0.0f;
                }
                if(returnHsl) {
                    return new float[] { hue, saturation, lightness };
                } else {
                    return hslToRgb(hue, saturation, lightness);
                }
            }

            else if(colorParams.equals("beach") == true) {
                float hue;

                int rgb = rnd.nextInt(2);

                if(rgb == 0) {
                    hue = 0.66f + rnd.nextFloat() * -0.2f;
                }
                else {
                    hue = 0.16f;
                    lightness = 0.5f + rnd.nextFloat() * 0.4f;
                }
                if(returnHsl) {
                    return new float[] { hue, saturation, lightness };
                } else {
                    return hslToRgb(hue, saturation, lightness);
                }

            }

            else if(colorParams.equals("goldsilverbronze") == true) {
                float hue;

                int rgb = rnd.nextInt(3);

                if(rgb == 0) {
                    hue = 0.138f;
//                    saturation = 0.91f;
  //                  lightness = 0.65f;
                    saturation = 0.99f;
                    lightness = 0.5f;

                }
                else if(rgb == 1) {
                    hue = 0.0f;
                    lightness = 0.85f;
                    saturation = 0.0f;
                }
                else {
                    hue = 0.075f;
                    saturation = 0.64f;
                    lightness = 0.69f;
                }
                if(returnHsl) {
                    return new float[] { hue, saturation, lightness };
                } else {
                    return hslToRgb(hue, saturation, lightness);
                }
            }

            else if(colorParams.equals("fluro") == true) {
                float hue;

                int rgb = rnd.nextInt(4);

                // yellow
                if(rgb == 0) {
                    hue = 0.333f / 2f;
                    saturation = 1f;
                    lightness = 0.5f;
                }

                // green
                else if(rgb == 1) {
                    hue = 0.333f;
                    saturation = 1.0f;
                    lightness = 0.5f;
                }

                // orange
                else if(rgb == 2) {
                    hue = 0.333f / 2f / 2f;
                    saturation = 1.0f;
                    lightness = 0.5f;
                }

                // pink
                else {
                    hue = 297 / 360f;
                    saturation = 0.8f;
                    lightness = 0.64f;
                }
                if(returnHsl) {
                    return new float[] { hue, saturation, lightness };
                } else {
                    return hslToRgb(hue, saturation, lightness);
                }
            }

        }

        else if(arrayContainsString(SCHEMES_TYPE_4, colorParams)) {

            float hueMin;
            float oneSixth = 1.0f / 6.0f;

            if(colorParams.equals("redyellow") == true) {
                hueMin = 0 * oneSixth;
            } else if(colorParams.equals("yellowgreen") == true) {
                hueMin = 1 * oneSixth;
            } else if(colorParams.equals("greencyan") == true) {
                hueMin = 2 * oneSixth;
            } else if(colorParams.equals("cyanblue") == true) {
                hueMin = 3 * oneSixth;
            } else if(colorParams.equals("bluemagenta") == true) {
                hueMin = 4 * oneSixth;
            } else {
                hueMin = 5 * oneSixth;
            }

            float hue = hueMin + rnd.nextFloat() * oneSixth;
            saturation = 0.5f + rnd.nextFloat() * 0.5f;
            lightness = 0.33f + rnd.nextFloat() * 0.33f;

            if(returnHsl) {
                return new float[] { hue, saturation, lightness };
            } else {
                return hslToRgb(hue, saturation, lightness);
            }

        }

        return null;
    }

    private static boolean arrayContainsString(String[] strings, String string) {
        for(int i = 0; i < strings.length; i++) {
            if(strings[i].equals(string) == true) {
                return true;
            }
        }
        return false;
    }

    private static float getBase() {
		// 100 buckets per day
		int SECONDS_PER_BUCKET = (60 * 60 * 24) / 100;
        Calendar c = Calendar.getInstance();
        int utcOffset = c.get(Calendar.ZONE_OFFSET) + c.get(Calendar.DST_OFFSET);
        Long utcMilliseconds = c.getTimeInMillis() + utcOffset;
        float base = ((utcMilliseconds / (1000 * SECONDS_PER_BUCKET)) % 100) / 100f;
        return base;
    }
	
    private static float[] hslToRgb(float h, float s, float l) {
        float r;
        float g;
        float b;

        if (s == 0) {
            r = g = b = l; // achromatic
        } else {
            float q = l < 0.5f ? l * (1 + s) : l + s - l * s;
            float p = 2 * l - q;
            r = hue2rgb(p, q, h + 1f / 3f);
            g = hue2rgb(p, q, h);
            b = hue2rgb(p, q, h - 1f / 3f);
        }
        return new float[] { r , g , b };
    }


    private static float hue2rgb(float p, float q, float t) {
        if (t < 0f) t += 1f;
        if (t > 1f) t -= 1f;
        if (t < 1f / 6f) return p + (q - p) * 6f * t;
        if (t < 1f / 2f) return q;
        if (t < 2f / 3f) return p + (q - p) * (2f / 3f - t) * 6f;
        return p;
    }

}
