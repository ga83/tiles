package com.tuxware.mesh;

interface FpsLimiter {
    void limitFps();
}
