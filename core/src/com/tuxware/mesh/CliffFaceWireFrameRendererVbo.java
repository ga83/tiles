package com.tuxware.mesh;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.PerspectiveCamera;
import com.badlogic.gdx.graphics.VertexAttribute;
import com.badlogic.gdx.graphics.VertexAttributes;
import com.badlogic.gdx.graphics.glutils.ShaderProgram;
import com.badlogic.gdx.graphics.glutils.VertexBufferObject;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.GdxRuntimeException;

public class CliffFaceWireFrameRendererVbo extends TriangleRenderer {

    private static final int NUMBER_OF_POSITION_COMPONENTS = 3;	// xyz
    private static final int NUMBER_OF_COLOR_COMPONENTS = 3;		// rgba
    private static final int NUMBER_OF_NORMAL_COMPONENTS = 3;	// xyz
    private static final int NUMBER_OF_BARY_COMPONENTS = 3;

    private static final int NUMBER_OF_VERTEX_COMPONENTS = NUMBER_OF_POSITION_COMPONENTS + NUMBER_OF_COLOR_COMPONENTS + NUMBER_OF_NORMAL_COMPONENTS + NUMBER_OF_BARY_COMPONENTS;

    private final VertexAttributes vAs;
    protected static float[] verts;
    protected static int index = 0;
    private VertexBufferObject vbo;


    @Override
    public void render(KirraArrayList<Triangle> triangles) {

//        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        // we can use depth buffer with this, because discarded fragments (the insides of triangles) don't write to the depth buffer. it looks better than blending.
        Gdx.gl.glClear(GL20.GL_DEPTH_BUFFER_BIT | GL20.GL_COLOR_BUFFER_BIT);


        shaderProgram.begin();

        shaderProgram.setUniform3fv(LightPosLocation,lightPos,0,3);
        shaderProgram.setUniformMatrix(u_ProjTransLocation, cam.combined);
        Gdx.gl.glDrawArrays(GL20.GL_TRIANGLES, 0, vbo.getNumVertices());

        shaderProgram.end();
    }


    protected void addTriangleToVbo(Triangle triangle) {

        Vector3 triangleNormal = triangle.n1;

        float r = triangle.color.r;
        float g = triangle.color.g;
        float b = triangle.color.b;

        //bottom left vertex
        verts[index++] = triangle.p1.x; 			//Position(x, y)
        verts[index++] = triangle.p1.y;
        verts[index++] = triangle.p1.z;

        verts[index++] = r; 	//Color(r, g, b, a)
        verts[index++] = g;
        verts[index++] = b;

        verts[index++] = triangleNormal.x;
        verts[index++] = triangleNormal.y;
        verts[index++] = triangleNormal.z;

        // barycentric
        verts[index++] = 1.0f;
        verts[index++] = 0.0f;
        verts[index++] = 0.0f;

        //top left vertex
        verts[index++] = triangle.p2.x; 			//Position(x, y)
        verts[index++] = triangle.p2.y;
        verts[index++] = triangle.p2.z;

        verts[index++] = r; 	//Color(r, g, b, a)
        verts[index++] = g;
        verts[index++] = b;

        verts[index++] = triangleNormal.x;
        verts[index++] = triangleNormal.y;
        verts[index++] = triangleNormal.z;

        // barycentric
        verts[index++] = 0.0f;
        verts[index++] = 1.0f;
        verts[index++] = 0.0f;

        //bottom right vertex
        verts[index++] = triangle.p3.x;	 //Position(x, y)
        verts[index++] = triangle.p3.y;
        verts[index++] = triangle.p3.z;

        verts[index++] = r; 	//Color(r, g, b, a)
        verts[index++] = g;
        verts[index++] = b;

        verts[index++] = triangleNormal.x;
        verts[index++] = triangleNormal.y;
        verts[index++] = triangleNormal.z;

        // barycentric
        verts[index++] = 0.0f;
        verts[index++] = 0.0f;
        verts[index++] = 1.0f;
    }

    @Override
    public void finalize() {
        vbo.dispose();
        shaderProgram.dispose();
    }



    public CliffFaceWireFrameRendererVbo(PerspectiveCamera cam, KirraArrayList<Triangle> triangles) {

        verts = new float[triangles.size() * 3 * NUMBER_OF_VERTEX_COMPONENTS];
        index = 0;

        for (int i = 0; i < triangles.size(); i++) {
            Triangle t1 = triangles.get(i);
            addTriangleToVbo(t1);
        }

        this.cam = cam;

        VertexAttribute vA = new VertexAttribute( VertexAttributes.Usage.Position, 3, "a_position" );
        VertexAttribute vC = new VertexAttribute( VertexAttributes.Usage.Position, 3, "a_color" );
        VertexAttribute vN = new VertexAttribute( VertexAttributes.Usage.Position, 3, "a_normal" );
        VertexAttribute vB = new VertexAttribute( VertexAttributes.Usage.Position, 3, "barycentric" );

        this.vAs = new VertexAttributes( new VertexAttribute[]{ vA, vC, vN, vB } );
        this.vbo = new VertexBufferObject(false, verts.length / NUMBER_OF_VERTEX_COMPONENTS, this.vAs);

        String vertexShaderString = Gdx.files.internal("vertex-infotech.glsl").readString();
        String fragmentShaderString = Gdx.files.internal("fragment-infotech-wireframe.glsl").readString();

        float distance = -cam.position.z;
//        fragmentShaderString = fragmentShaderString.replaceAll("__AMPLITUDE__FACTOR__", String.valueOf(4000.0 * distance) );
        fragmentShaderString = fragmentShaderString.replaceAll("__AMPLITUDE__FACTOR__", String.valueOf(3000.0 * Math.pow(distance, 1.05)) );

        ShaderProgram.pedantic = false;
        shaderProgram = new ShaderProgram(vertexShaderString,fragmentShaderString);

        this.vbo.bind(shaderProgram);
        this.vbo.setVertices(verts, 0, verts.length);

        String log = shaderProgram.getLog();
        if (shaderProgram.isCompiled() == false)
            throw new GdxRuntimeException(log);

        if (log != null)
            System.out.println("Shader Log: " + log);

        this.getUniformLocations();

    }


}
