package com.tuxware.mesh;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.FPSLogger;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.PerspectiveCamera;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.GdxRuntimeException;

import java.util.Arrays;
import java.util.List;
import java.util.Random;


// TODO: there is still a serious bug regarding empty or unloadable pictures. copy logic for loading images wholesale from collage app!!!!!!!!!!!
// TODO: there is still a serious bug regarding empty or unloadable pictures. copy logic for loading images wholesale from collage app!!!!!!!!!!!
// TODO: there is still a serious bug regarding empty or unloadable pictures. copy logic for loading images wholesale from collage app!!!!!!!!!!!
// TODO: there is still a serious bug regarding empty or unloadable pictures. copy logic for loading images wholesale from collage app!!!!!!!!!!!
// TODO: there is still a serious bug regarding empty or unloadable pictures. copy logic for loading images wholesale from collage app!!!!!!!!!!!
// TODO: there is still a serious bug regarding empty or unloadable pictures. copy logic for loading images wholesale from collage app!!!!!!!!!!!
// TODO: there is still a serious bug regarding empty or unloadable pictures. copy logic for loading images wholesale from collage app!!!!!!!!!!!
// TODO: there is still a serious bug regarding empty or unloadable pictures. copy logic for loading images wholesale from collage app!!!!!!!!!!!
// TODO: there is still a serious bug regarding empty or unloadable pictures. copy logic for loading images wholesale from collage app!!!!!!!!!!!
// TODO: there is still a serious bug regarding empty or unloadable pictures. copy logic for loading images wholesale from collage app!!!!!!!!!!!
// TODO: there is still a serious bug regarding empty or unloadable pictures. copy logic for loading images wholesale from collage app!!!!!!!!!!!
// TODO: there is still a serious bug regarding empty or unloadable pictures. copy logic for loading images wholesale from collage app!!!!!!!!!!!
// TODO: there is still a serious bug regarding empty or unloadable pictures. copy logic for loading images wholesale from collage app!!!!!!!!!!!
// TODO: there is still a serious bug regarding empty or unloadable pictures. copy logic for loading images wholesale from collage app!!!!!!!!!!!
// TODO: there is still a serious bug regarding empty or unloadable pictures. copy logic for loading images wholesale from collage app!!!!!!!!!!!
// TODO: there is still a serious bug regarding empty or unloadable pictures. copy logic for loading images wholesale from collage app!!!!!!!!!!!
// TODO: there is still a serious bug regarding empty or unloadable pictures. copy logic for loading images wholesale from collage app!!!!!!!!!!!
// TODO: there is still a serious bug regarding empty or unloadable pictures. copy logic for loading images wholesale from collage app!!!!!!!!!!!
// TODO: there is still a serious bug regarding empty or unloadable pictures. copy logic for loading images wholesale from collage app!!!!!!!!!!!
// TODO: there is still a serious bug regarding empty or unloadable pictures. copy logic for loading images wholesale from collage app!!!!!!!!!!!
// TODO: there is still a serious bug regarding empty or unloadable pictures. copy logic for loading images wholesale from collage app!!!!!!!!!!!
// TODO: there is still a serious bug regarding empty or unloadable pictures. copy logic for loading images wholesale from collage app!!!!!!!!!!!
// TODO: there is still a serious bug regarding empty or unloadable pictures. copy logic for loading images wholesale from collage app!!!!!!!!!!!
// TODO: there is still a serious bug regarding empty or unloadable pictures. copy logic for loading images wholesale from collage app!!!!!!!!!!!
// TODO: there is still a serious bug regarding empty or unloadable pictures. copy logic for loading images wholesale from collage app!!!!!!!!!!!
// TODO: there is still a serious bug regarding empty or unloadable pictures. copy logic for loading images wholesale from collage app!!!!!!!!!!!
// TODO: there is still a serious bug regarding empty or unloadable pictures. copy logic for loading images wholesale from collage app!!!!!!!!!!!
// TODO: there is still a serious bug regarding empty or unloadable pictures. copy logic for loading images wholesale from collage app!!!!!!!!!!!
// TODO: there is still a serious bug regarding empty or unloadable pictures. copy logic for loading images wholesale from collage app!!!!!!!!!!!
// TODO: there is still a serious bug regarding empty or unloadable pictures. copy logic for loading images wholesale from collage app!!!!!!!!!!!
// TODO: there is still a serious bug regarding empty or unloadable pictures. copy logic for loading images wholesale from collage app!!!!!!!!!!!
// TODO: there is still a serious bug regarding empty or unloadable pictures. copy logic for loading images wholesale from collage app!!!!!!!!!!!
// TODO: there is still a serious bug regarding empty or unloadable pictures. copy logic for loading images wholesale from collage app!!!!!!!!!!!



// TODO: voronoi with simple elevation of centre points
// TODO: delaunay triangulation terrain - https://straypixels.net/delaunay-triangulation-terrain/
// TODO: minecraft-style terrain made of cubes
public class MeshWallpaper extends ApplicationAdapter implements ApplicationListener {

	// TODO: values should be different for collage schemes. since they load lots of textures, the grid size should be about -15 to +15, not -22 to +22.
	// TODO: but in order to satisfy that, these constants below need to change only for collage schemes. or variables should be used.
	private static final float X_MIN_CAMERA_BOUNCE = -20000;
	private static final float X_MAX_CAMERA_BOUNCE =  20000;
	private static final float Y_MIN_CAMERA_BOUNCE = -20000;
	private static final float Y_MAX_CAMERA_BOUNCE =  20000;

	private static final float X_MIN_LOOKAT_BOUNCE = (float) (X_MIN_CAMERA_BOUNCE * 0.75);
	private static final float X_MAX_LOOKAT_BOUNCE = (float) (X_MAX_CAMERA_BOUNCE * 0.75);
	private static final float Y_MIN_LOOKAT_BOUNCE = (float) (Y_MIN_CAMERA_BOUNCE * 0.75);
	private static final float Y_MAX_LOOKAT_BOUNCE = (float) (Y_MAX_CAMERA_BOUNCE * 0.75);

	private static final float X_MIN_CAMERA_RESET = X_MIN_CAMERA_BOUNCE * 2.0f;
	private static final float X_MAX_CAMERA_RESET = X_MAX_CAMERA_BOUNCE * 2.0f;
	private static final float Y_MIN_CAMERA_RESET = Y_MIN_CAMERA_BOUNCE * 2.0f;
	private static final float Y_MAX_CAMERA_RESET = Y_MAX_CAMERA_BOUNCE * 2.0f;

	private static final float X_MIN_LOOKAT_RESET = X_MIN_LOOKAT_BOUNCE * 2.0f;
	private static final float X_MAX_LOOKAT_RESET = X_MAX_LOOKAT_BOUNCE * 2.0f;
	private static final float Y_MIN_LOOKAT_RESET = Y_MIN_LOOKAT_BOUNCE * 2.0f;
	private static final float Y_MAX_LOOKAT_RESET = Y_MAX_LOOKAT_BOUNCE * 2.0f;

	private static final float LOOKAT_SPEED = 300;   // units per second

	private static final float MAX_CAMERA_STEER_ANGULAR_VELOCITY = 10.0f;
	private static final float MAX_LOOKAT_STEER_ANGULAR_VELOCITY = 10.0f;
	public static final int LOOK_AT_DISTANCE = 32000;
	private static final long CAMERA_CHECK_INTERVAL = 100;


	private static FPSLogger fps1;

    private static KirraArrayList<Triangle> triangles;	// just a reference to the triangle list stored in the mesh scheme, because the scheme is responsible for updating it

	private PerspectiveCamera cam;

	public static Random rnd = new Random();

	private Vector3 lookAtPosition;
	private Vector3 cameraPosition;

	private static Vector3 lookAtVelocity;
	private static Vector3 cameraVelocity;

    private static float cameraSteerAngularVelocity = 0;
    private static float lookAtSteerAngularVelocity = 0;

    private Vector3 rotationAxis = new Vector3(0,0,1);

	protected static int sleepDuration = 0;

	private Preferences prefs;

	public static boolean preferencesChanged = false;

	private float currentTime = 0.0f;
	private float lastUpdateTime = 0.0f;

	private float filled;
	private String colorScheme;
	private String shape;

	private TriangleRenderer triangleRenderer;

	private Colorer colorer;
	private String style;
	private float fov;
	private float cameraDistance;
	private String nonFilledColor;
	private String material;

	private KirraArrayList<Texture> textures;
	private String imagePath;
	private FpsLimiter fpsLimiter;
	private boolean isCollage;
	private ResizeStrategy resizeStrategy;
    private int minImageResolution;
	private int maxImagesLoad;
	private int textureQuality;

	private int fps;
	private float cameraSpeed;
	private Vector3 upVector;
	private static MeshWallpaper aa;
	private String tileType;
	private TileGenerator tileGenerator;
	private Float margin;
	private int lastWidth;
	private int lastHeight;
	private long lastCameraCheck;
	private long currentCameraTime;


	@Override
	public void create() {

		this.initialisePreferences();
		this.loadPreferences();

		this.fps1 = new FPSLogger();
		this.upVector = new Vector3(0.0f, 1.0f, 0.0f);

		TilesUsageLogger logger = new TilesUsageLogger();
		logger.setApplicationName("tiles");
		logger.setUrl("http://!.!.!.!/logappusage.php");

		try {
			logger.log();
		} catch (ParametersNotSetException e) {
			e.printStackTrace();
		}

		aa = this;

	}

	private void initialisePreferences() {

		prefs = Gdx.app.getPreferences("prefs");

		String colorScheme = prefs.getString("colorscheme");
		String filled = prefs.getString("filled");
		String shape = prefs.getString("shape");
		String style = prefs.getString("style");
		String fov = prefs.getString("fov");
		String cameraDistance = prefs.getString("cameradistance");
		String nonFilledColor = prefs.getString("nonfilledcolor");
		String material = prefs.getString("material");
		String resizeStrategy = prefs.getString("resizestrategy");
		String minImageResolution = prefs.getString("minimageresolution");
		String maxImagesLoad = prefs.getString("maximagesload");
		String textureQuality = prefs.getString("texturequality");
		String fps = prefs.getString("fps");
		String cameraSpeed = prefs.getString("cameraspeed");
		String aaSamples = prefs.getString("aasamples");
		String tileType = prefs.getString("tiletype");
		String margin = prefs.getString("tilegap");



		if(colorScheme.isEmpty() == true || colorScheme == null) {
			prefs.putString("colorscheme","redyellow");
		}

		if(filled.isEmpty() == true || filled == null) {
			prefs.putString("filled","60");
		}

		if(shape.isEmpty() == true || shape == null) {
			prefs.putString("shape","motel");
		}

		if(style.isEmpty() == true || style == null) {
			prefs.putString("style","random");
		}

		if(fov.isEmpty() == true || fov == null) {
			prefs.putString("fov","90");
		}

		if(cameraDistance.isEmpty() == true || cameraDistance == null) {
			prefs.putString("cameradistance","3000");
		}

		if(nonFilledColor.isEmpty() == true || nonFilledColor == null) {
			prefs.putString("nonfilledcolor","black");
		}

		if(material.isEmpty() == true || material == null) {
			prefs.putString("material","plastic");
		}

		if(resizeStrategy.isEmpty() == true || resizeStrategy == null) {
			prefs.putString("resizestrategy","crop");
		}

        if(minImageResolution.isEmpty() == true || minImageResolution == null) {
            prefs.putString("minimageresolution","500");
        }

		if(maxImagesLoad.isEmpty() == true || maxImagesLoad == null) {
			prefs.putString("maximagesload","10");
		}

		if(textureQuality.isEmpty() == true || textureQuality == null) {
			prefs.putString("texturequality","256");
		}

		if(fps.isEmpty() == true || fps == null) {
			prefs.putString("fps","60");
		}

		if(cameraSpeed.isEmpty() == true || cameraSpeed == null) {
			prefs.putString("cameraspeed","600.0");
		}

		if(aaSamples.isEmpty() == true || aaSamples == null) {
			prefs.putString("aasamples","2");
		}

		if(tileType.isEmpty() == true || tileType == null) {
			prefs.putString("tiletype","sharp");
		}

		if(margin.isEmpty() == true || margin == null) {
			prefs.putString("tilegap","5");
		}

		prefs.flush();
	}


	private void setupDepth() {
		// TODO: depth testing is not working properly. it seems to half work (it does make a difference turning it on), but still some distant faces get drawn on top of closer ones.
		// TODO: seems like we can't have blending and depth testing at the same time. shouldn't need blending though.
		Gdx.gl.glDepthMask(true);
		Gdx.gl.glDisable(GL20.GL_BLEND);
		Gdx.gl.glEnable(GL20.GL_DEPTH_TEST);
		Gdx.gl.glDepthRangef(0.0f,1.0f);
		Gdx.gl.glClearDepthf(4.0f);
		Gdx.gl.glClearColor(0, 0, 0.0f, 1);
		Gdx.gl.glDepthFunc(GL20.GL_LESS);
		Gdx.gl.glClear(GL20.GL_DEPTH_BUFFER_BIT);
	}

	private void setupNoDepth() {
		Gdx.gl.glDepthMask(false);
		Gdx.gl.glEnable(GL20.GL_BLEND);
		Gdx.gl.glBlendFunc(GL20.GL_ONE, GL20.GL_ONE);
		Gdx.gl.glDisable(GL20.GL_DEPTH_TEST);
		Gdx.gl.glClearColor(0, 0, 0.0f, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
	}


	@Override
	public void render() {

		if(preferencesChanged == true) {
			loadPreferences();
			preferencesChanged = false;
		}

		fpsLimiter.limitFps();

		float dt = Gdx.graphics.getRawDeltaTime();

		currentTime += dt;

		// only check these things at a fixed timestep, not every frame
		// strictly, we should run the physics updates as many times as
		// there are seconds since last update, but it is unlikely there
		// would be more than 1 without having to do a hard reset of
		// camera and lookat
		if(currentTime > lastUpdateTime + 1.0f) {
			randomiseLookAtVelocity();
			randomiseCameraVelocity();
			checkOutOfBounds();

			lastUpdateTime = currentTime;
		}

		// don't check every frame, it would use too much cpu
		long currentCameraTime = System.currentTimeMillis();
		if(currentCameraTime > this.lastCameraCheck + MeshWallpaper.CAMERA_CHECK_INTERVAL) {
			this.checkCamera();
			this.lastCameraCheck = currentCameraTime;
		}

		updateCameraAndLookAtPosition(dt);


		cam.position.x = cameraPosition.x;
		cam.position.y = cameraPosition.y;
		cam.position.z = cameraPosition.z;

		cam.lookAt(lookAtPosition.x, lookAtPosition.y, lookAtPosition.z);
		cam.up.set(this.upVector);


		cam.update(true);

		triangleRenderer.render(triangles);

		fps1.log();

	}


	public static void restart() {
		if(aa != null) {
			System.exit(0);
		}
	}


	public void loadPreferences() {

		prefs = Gdx.app.getPreferences("prefs");

		// TODO: voronoi is not easy to use, but it's possible.
		// TODO: for example, only some points (in the middle of the image mostly) are actually surrounded by edges
		// TODO: on all sides. we have to generate some edges manually to make every point, especially the ones
		// TODO: near the edges, be surrounded by lines.
		// TODO: for now, not using voronoi. try generate a pyramid instead.
		// TODO: https://robertlovespi.net/2014/06/16/five-versions-of-a-tessellation-using-squares-and-equilateral-triangles/

		colorScheme = prefs.getString("colorscheme","redgreenblue");
		filled = Integer.valueOf(prefs.getString("filled","20")) / 100.0f;
		shape = prefs.getString("shape","square");
		style = prefs.getString("style");
		fov = Float.valueOf(prefs.getString("fov"));
		cameraDistance = Float.valueOf(prefs.getString("cameradistance"));
		nonFilledColor = prefs.getString("nonfilledcolor");
		material = prefs.getString("material");
		imagePath = prefs.getString("imagepath") + "/";
        minImageResolution = Integer.valueOf(prefs.getString("minimageresolution"));
		maxImagesLoad = Integer.valueOf(prefs.getString("maximagesload"));
		textureQuality = Integer.valueOf(prefs.getString("texturequality"));
		fps = Integer.valueOf(prefs.getString("fps"));
		cameraSpeed = Float.valueOf(prefs.getString("cameraspeed"));
		tileType = prefs.getString("tiletype", "sharp");
		margin = Float.valueOf(prefs.getString("tilegap"));



		if(prefs.getString("resizestrategy").equals("crop") == true) {
			this.resizeStrategy = ResizeStrategy.CROP;
		}
		else if(prefs.getString("resizestrategy").equals("blackborders") == true) {
			this.resizeStrategy = ResizeStrategy.BLACKBORDERS;
		}
		else if(prefs.getString("resizestrategy").equals("blurborders") == true) {
			this.resizeStrategy = ResizeStrategy.BLURBORDERS;
		}

		// TODO: extend if we have more collage schemes
		isCollage = shape.equals("squarecollage") == true || shape.equals("squarecollageflat") == true;

		if(fps == 15) {
			this.fpsLimiter = new FpsLimiter15();
		}
		else if(fps == 30) {
			this.fpsLimiter = new FpsLimiter30();
		}
		else if(fps == 45) {
			this.fpsLimiter = new FpsLimiter45();
		} else {
			this.fpsLimiter = new FpsLimiterVsync();
		}

		KirraArrayList<String> textureFilePaths = null;

		// don't allow to just use everything on the device because it's too slow to recurse through
		if(imagePath.equals("/") == false) {
			textureFilePaths = getTextureFilePaths(imagePath);
		}

		initialiseCameraPositionAndVelocity();
		initialiseLookAtPositionAndVelocity();


		//this.resetCamera(true);
		this.setCamera(false);

		com.tuxware.mesh.Color.setNonFilledColor(nonFilledColor);

        // create colorer
        if(style.equals("pools") == true) {
			colorer = new PoolsColorer(colorScheme);
		} else if(style.equals("perlin") == true) {
			colorer = new PerlinColorer(colorScheme);
		} else if(style.equals("random") == true) {
			colorer = new RandomColorer(colorScheme);
		} else if(style.equals("hueshift") == true) {
			colorer = new HueShiftColorer(colorScheme);
		}

        if(this.tileType.equals("sharp") == true) {
        	this.tileGenerator = new SharpTileGenerator();
		} else if(this.tileType.equals("smooth") == true) {
//        	this.tileGenerator = new SmoothTileGenerator();
			this.tileGenerator = new ObjectTileGenerator();
		} else if(this.tileType.equals("flat") == true) {
			this.tileGenerator = new FlatTileGenerator();
		}

		long startTime = System.currentTimeMillis();


		// create scheme, ie geometry
		if(shape.equals("hexagon") == true) {
            triangles = new HexagonGemScheme().create(filled, colorer, tileGenerator, margin);
        } else if(shape.equals("octagon") == true) {
                triangles = new OctagonGemScheme().create(filled, colorer, tileGenerator, margin);
		} else if(shape.equals("hexagondisc") == true) {
			triangles = new HexagonDiscScheme().create(filled, colorer, null, margin);
		} else if(shape.equals("square") == true) {
			triangles = new SquareGemScheme().create(filled, colorer, tileGenerator, margin);
		} else if(shape.equals("squaredisc") == true) {
			triangles = new SquareDiscScheme().create(filled, colorer, null, margin);
		} else if(shape.equals("cubes") == true) {
			triangles = new CubesStackedScheme().create(filled, colorer, null, margin);
		} else if(shape.equals("cubesfloating") == true) {
			triangles = new CubesFloatingScheme().create(filled, colorer, null, margin);
		} else if(shape.equals("neoncliffface") == true) {
			triangles = new HexagonsCliffFaceScheme().create(filled, colorer, null, margin);
		} else if(shape.equals("cliffface") == true) {
			triangles = new HexagonsCliffFaceScheme().create(filled, colorer, null, margin);
		} else if(shape.equals("duallayer") == true) {
			triangles = new HexagonsDualLayerScheme().create(filled, colorer, null, margin);
		} else if(shape.equals("cliffacewireframe") == true) {
			triangles = new HexagonsCliffFaceWireFrameScheme().create(filled, colorer, null, margin);
		} else if(shape.equals("landscape") == true) {
			triangles = new HexagonsLandscapeScheme().create(filled, colorer, null, margin);
		} else if(shape.equals("triangle1") == true) {
			triangles = new TriangleGemType1Scheme().create(filled, colorer, tileGenerator, margin);
		} else if(shape.equals("triangle2") == true) {
			triangles = new TriangleGemType1Scheme().create(filled, colorer, null, margin);
		} else if(shape.equals("diamond") == true) {
			triangles = new DiamondGemScheme().create(filled, colorer, null, margin);
		} else if(shape.equals("motel") == true) {
			triangles = new MotelTilesScheme().create(filled, colorer, null, margin);
		} else {
			// default, something went wrong so choose square gem scheme
			triangles = new SquareGemScheme().create(filled, colorer, null, margin); // default
		}

		long endTime = System.currentTimeMillis();
		System.out.println("aaa time to create: " + (endTime - startTime));


		// create renderer
		if(shape.equals("neoncliffface") == true) {
			triangleRenderer = new NeonCliffFaceRendererVbo(cam, triangles);
			setupDepth();
		} else if(shape.equals("cliffface") == true) {
			triangleRenderer = new CliffFaceRendererVbo(cam, triangles, material);
			setupDepth();
		} else if(shape.equals("duallayer") == true) {
			triangleRenderer = new DualLayerRendererVbo(cam, triangles);
			setupNoDepth();
		} else if(shape.equals("cliffacewireframe") == true) {
			triangleRenderer = new CliffFaceWireFrameRendererVbo(cam, triangles);
			// we can use depth buffer with this, because discarded fragments (the insides of triangles) don't write to the depth buffer. it looks better than blending.
			setupDepth();
		} else {
			triangleRenderer = new RegularRendererVbo(cam, triangles, material, shape);
			setupDepth();
		}
	}


	private static KirraArrayList<String> getTextureFilePaths(String imagePath) {
		FileHandle dirHandle = Gdx.files.absolute(imagePath);
		FileHandle[] list = dirHandle.list();
		List imageExtensions = Arrays.asList(new String[] { "png", "jpg" });
		KirraArrayList<String> textureFilePaths = new KirraArrayList<String>(10000);

		for(int i = 0; i < list.length; i++) {
			FileHandle fileHandle = list[i];
			String fileOrFolderPath = imagePath + fileHandle.name();
			boolean isDirectory = fileHandle.isDirectory();
			boolean isImage = imageExtensions.contains(fileHandle.extension());

			if(isDirectory == true) {
				// this is a folder, so look inside it recursively
				fileOrFolderPath += "/";
				KirraArrayList<String> textureFilePathsInsideFolder = getTextureFilePaths(fileOrFolderPath);
				textureFilePaths.addAll(textureFilePathsInsideFolder);

			} else if(isDirectory == false && isImage == true) {
				// this is just a file, so add it to list
				textureFilePaths.add(fileOrFolderPath);
			}
		}

		// shuffle list, because only a certain number can be loaded in memory, and we don't want the same images loaded every time
		//Collections.shuffle(textureFilePaths);

		return textureFilePaths;
	}


	enum ResizeStrategy {
		CROP, BLACKBORDERS, BLURBORDERS
	}


	// TODO: there is still a serious bug regarding empty or unloadable pictures. copy logic for loading images wholesale from collage app!!!!!!!!!!!
	private KirraArrayList<Texture> loadTexturesFromDisk(KirraArrayList<String> texturePaths, ResizeStrategy resizeStrategy) {

		if(this.textures != null) {
			for( ; this.textures.size() > 0; ) {
				Texture texture = this.textures.get(0);
				this.textures.remove(0);
				texture.dispose();
			}
		} else {
			this.textures = new KirraArrayList<Texture>(100);
		}

		// if user selected no paths yet, just put the one warning texture in there
		if(texturePaths == null) {
			FileHandle noMargot = Gdx.files.internal("selectfolder.png");
			Pixmap pixmap = new Pixmap(noMargot);
			Texture selectFolder = new Texture(pixmap);
			pixmap.dispose();
			this.textures.add(selectFolder);
			return this.textures;
		}

		boolean loadingLimitHit = false;

		for(int i = 0; i < texturePaths.size() && loadingLimitHit == false; i++) {

			FileHandle margot = Gdx.files.absolute(texturePaths.get(i));

			// TODO: come up with a criteria which perhaps takes into account memory used instead of number of files loaded
			if(this.textures.size() == maxImagesLoad) {
				loadingLimitHit = true;
			}

			if(margot != null) {

				try {
					Pixmap pixmapOriginalSize = new Pixmap(margot);

					int width = pixmapOriginalSize.getWidth();
					int height = pixmapOriginalSize.getHeight();

					if(width < minImageResolution || height < minImageResolution) {
						// don't load the image if it doesn't pass the minimum resolution requirement
					    continue;
                    }


					// the scale depends on the resize strategy.
					// ie, if adding borders, the longest side needs to be below the max height or width.
					// if cropping, only the shortest side needs to be below max height or width.
					float scale = getScale(width, height, resizeStrategy);

					int reducedWidth = (int) (width / scale);
					int reducedHeight = (int) (height / scale);

					// make copy of pixmap, which is smaller
					Pixmap pixmapScaled = new Pixmap(reducedWidth,reducedHeight, Pixmap.Format.RGBA8888);
					pixmapScaled.drawPixmap(pixmapOriginalSize, 0, 0, width, height, 0, 0, reducedWidth, reducedHeight);

					Pixmap pixmapScaledAndCropped;

					// if not square, decide how to crop or add borders to the image. if square, just return original pixmap
					if(reducedHeight != reducedWidth) {
						if (resizeStrategy == ResizeStrategy.CROP) {
							pixmapScaledAndCropped = getCroppedPixmap(pixmapScaled);
							pixmapScaled.dispose();
						} else if (resizeStrategy == ResizeStrategy.BLACKBORDERS) {
							pixmapScaledAndCropped = getBordersPixmap(pixmapScaled);
							pixmapScaled.dispose();
						} else { // ie - resizeStrategy == ResizeStrategy.BLURBORDERS {
							pixmapScaledAndCropped = getBlurBordersPixmap(pixmapScaled);
							pixmapScaled.dispose();
						}
					} else {
						pixmapScaledAndCropped = pixmapScaled;
					}

					Texture resizedTexture = new Texture(pixmapScaledAndCropped);
					pixmapOriginalSize.dispose();
//					pixmapScaled.dispose();

					// TODO: IF opengl context losses (upon pause and resume) are frequent, we shouldn't dispose the texture here.
					// TODO: in fact, we should keep the Pixmap around so that Texture objects can be recreated from them on resume.
					// TODO: this step is necessary in the case of context loss because Textures created from Pixmaps are not "managed" textures like ones loaded from files
					// TODO: supposedly, opengl context loss should be increasingly rare so this may not be necessary. read this:
					// TODO: https://badlogicgames.com/forum/viewtopic.php?f=11&t=20757
					// TODO: of course, if opengl context loss is very rare, we should go ahead and dispose the pixmaps, because they take up several hundred mb in total.
					// TODO: also read:
					// TODO: https://www.badlogicgames.com/wordpress/?p=1073
					// TODO: also read:
					// TODO: https://www.badlogicgames.com/wordpress/?p=367
					pixmapScaledAndCropped.dispose();

					this.textures.add(resizedTexture);
				}
				// catch primarily to catch images which can't load for any reason at all... eg too large.
				catch (GdxRuntimeException e) {
					System.out.println("aaa caught exception on file " + texturePaths.get(i) + ", not loading");
				}

			}
		}

		return this.textures;
	}


	private Pixmap getBordersPixmap(Pixmap pixmapScaled) {
		Pixmap pixmapWithBorders;

		if(pixmapScaled.getWidth() > pixmapScaled.getHeight()) {
			int increasedHeight = pixmapScaled.getWidth();
			int heightDifference = increasedHeight - pixmapScaled.getHeight();
			int yOffset = heightDifference / 2;
			pixmapWithBorders = new Pixmap(pixmapScaled.getWidth(), increasedHeight, Pixmap.Format.RGBA8888);
			pixmapWithBorders.setColor(com.badlogic.gdx.graphics.Color.BLACK);
			pixmapWithBorders.fill();
			pixmapWithBorders.drawPixmap(pixmapScaled,0,yOffset);

		} else if(pixmapScaled.getHeight() > pixmapScaled.getWidth()) {
			int increasedWidth = pixmapScaled.getHeight();
			int widthDifference = increasedWidth - pixmapScaled.getWidth();
			int xOffset = widthDifference / 2;
			pixmapWithBorders = new Pixmap(increasedWidth, pixmapScaled.getHeight() , Pixmap.Format.RGBA8888);
			pixmapWithBorders.setColor(com.badlogic.gdx.graphics.Color.BLACK);
			pixmapWithBorders.fill();
			pixmapWithBorders.drawPixmap(pixmapScaled,xOffset,0);
		} else {
			pixmapWithBorders = pixmapScaled;
		}

		return pixmapWithBorders;
	}


	private Pixmap getCroppedPixmap(Pixmap pixmapScaled) {
		Pixmap pixmapScaledAndCropped;

		if(pixmapScaled.getWidth() > pixmapScaled.getHeight()) {
			int croppedReducedWidth = pixmapScaled.getHeight();
			int widthDifference = pixmapScaled.getWidth() - croppedReducedWidth;
			int xOffset = 0 - widthDifference / 2;

			pixmapScaledAndCropped = new Pixmap(croppedReducedWidth, pixmapScaled.getHeight(), Pixmap.Format.RGBA8888);
			pixmapScaledAndCropped.drawPixmap(pixmapScaled,xOffset,0);

		} else if(pixmapScaled.getHeight() > pixmapScaled.getWidth()) {
			int croppedReducedHeight = pixmapScaled.getWidth();
			int heightDifference = pixmapScaled.getHeight() - croppedReducedHeight;
			int yOffset = 0 - heightDifference;
			pixmapScaledAndCropped = new Pixmap(pixmapScaled.getWidth(), croppedReducedHeight, Pixmap.Format.RGBA8888);
			pixmapScaledAndCropped.drawPixmap(pixmapScaled,0,yOffset);
		} else {
			pixmapScaledAndCropped = pixmapScaled;
		}
		return pixmapScaledAndCropped;
	}

	private Pixmap getBlurBordersPixmap(Pixmap pixmapScaled) {
		return getBordersPixmap(pixmapScaled);
	}

	// probably only want integer scale, because it looks best
	private float getScale(int width, int height, ResizeStrategy resizeStrategy) {

		float scale;

		if(resizeStrategy == ResizeStrategy.CROP) {
			scale = 1.0f;

			while (
					width / scale > textureQuality ||
					height / scale > textureQuality
			) {
				scale += 0.1f;
			}
		}
		else { // resizeStrategy == ResizeStrategy.BLACKBORDERS or resizeStrategy == ResizeStrategy.BLURBORDERS
			scale = 1.0f;

			while (
					width / scale > textureQuality &&
					height / scale > textureQuality
			) {
				scale += 0.1f;
			}
		}

		return scale;
	}




	private void initialiseLookAtPositionAndVelocity() {

		float lookAtDistanceToUse = LOOK_AT_DISTANCE;

		// collage schemes should be less slanted so that the images are more legible.
		if(isCollage == true) {
//			lookAtDistanceToUse *= 4.0;
		}

        lookAtPosition = new Vector3(0, 0, lookAtDistanceToUse);

        Vector2 lookAtVelocity2d = new Vector2().setToRandomDirection().scl(LOOKAT_SPEED);

        lookAtVelocity = new Vector3();
        lookAtVelocity.x = lookAtVelocity2d.x;
        lookAtVelocity.y = lookAtVelocity2d.y;
    }

    private void initialiseCameraPositionAndVelocity() {

		float cameraDistanceToUse = this.cameraDistance;

		// collage schemes should be more zoomed in so that the image is much larger on screen.
		if(isCollage == true) {
			cameraDistanceToUse /= 4.0;
		} else {
//				cameraDistanceToUse *= 4.0;
		}

//		cameraDistanceToUse /= 10.0;

		cameraPosition = new Vector3(0, 0, -cameraDistanceToUse);

        Vector2 cameraVelocity2d = new Vector2().setToRandomDirection().scl(cameraSpeed);

        cameraVelocity = new Vector3();
        cameraVelocity.x = cameraVelocity2d.x;
        cameraVelocity.y = cameraVelocity2d.y;
    }

    private void randomiseCameraVelocity() {

		int sign;

		if(rnd.nextBoolean() == true) {
			sign = 1;
		} else {
			sign = -1;
		}

		cameraSteerAngularVelocity = rnd.nextFloat() * MAX_CAMERA_STEER_ANGULAR_VELOCITY * sign;

		if(cameraSteerAngularVelocity > MAX_CAMERA_STEER_ANGULAR_VELOCITY) {
			cameraSteerAngularVelocity = MAX_CAMERA_STEER_ANGULAR_VELOCITY;
		}
		else if(cameraSteerAngularVelocity < -MAX_CAMERA_STEER_ANGULAR_VELOCITY) {
			cameraSteerAngularVelocity = -MAX_CAMERA_STEER_ANGULAR_VELOCITY;
		}
    }

	@Override
	public void resume() {
		System.out.println("===== gg resume");
		this.cam.viewportWidth = Gdx.graphics.getWidth();
		this.cam.viewportHeight = Gdx.graphics.getHeight();
		this.cam.update();
	}

	private void checkCamera() {
		int width = Gdx.graphics.getWidth();
		int height = Gdx.graphics.getHeight();

		if(
				this.lastWidth != width ||
						this.lastHeight != height
		) {
			System.out.println("===== calling resetcamera");
			this.setCamera(true);
			this.lastWidth = width;
			this.lastHeight = height;
		}
	}

	private void setCamera(boolean resetRenderer) {
		int width = Gdx.graphics.getWidth();
		int height = Gdx.graphics.getHeight();

		float yFieldOfView;
		if(height > width) {
			yFieldOfView = fov;
		}
		else if(width > height) {
			yFieldOfView = fov * 0.66f;
		}
		else {
			yFieldOfView = fov * 0.8f;
		}

		// System.out.println("===== yfov: " + yFieldOfView + ", " + width + ", " + height);

		this.cam = new PerspectiveCamera(yFieldOfView, width, height);
		this.cam.position.set(this.cameraPosition.cpy());
		this.cam.lookAt(this.lookAtPosition);
		this.cam.near = 10f;   // must be greater than 0 otherwise depth test fails frequently
		this.cam.far = 18200f;
		this.cam.update();
		this.resetCamera(resetRenderer);

	}

	// check if the camera or the lookatl is out of bounds, and if so,
	// either bounce or reset them both, depending how far out of bounds it / they are
	private void checkOutOfBounds() {

		// check camera position
		if (cameraPosition.x < X_MIN_CAMERA_BOUNCE) {
			// bounce (repeated for all walls below)
			if(cameraVelocity.x < 0) {
				cameraVelocity.x *= -1;
			}
			// too far, reset (repeated for all walls below)
			if(cameraPosition.x < X_MIN_CAMERA_RESET) {
				resetCamera(true);
			}
		}

		if (cameraPosition.x > X_MAX_CAMERA_BOUNCE) {
			if(cameraVelocity.x > 0) {
				cameraVelocity.x *= -1;
			}

			if(cameraPosition.x > X_MAX_CAMERA_RESET) {
				resetCamera(true);
			}
		}

		if (cameraPosition.y < Y_MIN_CAMERA_BOUNCE) {
			if(cameraVelocity.y < 0) {
				cameraVelocity.y *= -1;
			}

			if(cameraPosition.y < Y_MIN_CAMERA_RESET) {
				resetCamera(true);
			}
		}

		if (cameraPosition.y > Y_MAX_CAMERA_BOUNCE) {
			if(cameraVelocity.y > 0) {
				cameraVelocity.y *= -1;
			}

			if(cameraPosition.y > Y_MAX_CAMERA_RESET) {
				resetCamera(true);
			}
		}


		// check lookat position
		if (lookAtPosition.x < X_MIN_LOOKAT_BOUNCE) {
			if (lookAtVelocity.x < 0) {
				lookAtVelocity.x *= -1;
			}

			if (lookAtPosition.x < X_MIN_LOOKAT_RESET) {
				resetCamera(true);
			}

		}

		if (lookAtPosition.x > X_MAX_LOOKAT_BOUNCE) {
			if (lookAtVelocity.x > 0) {
				lookAtVelocity.x *= -1;
			}

			if (lookAtPosition.x > X_MAX_LOOKAT_RESET) {
				resetCamera(true);
			}

		}

		if (lookAtPosition.y < Y_MIN_LOOKAT_BOUNCE) {
			if (lookAtVelocity.y < 0) {
				lookAtVelocity.y *= -1;
			}

			if (lookAtPosition.y < Y_MIN_LOOKAT_RESET) {
				resetCamera(true);
			}

		}

		if (lookAtPosition.y > Y_MAX_LOOKAT_BOUNCE) {
			if (lookAtVelocity.y > 0) {
				lookAtVelocity.y *= -1;
			}

			if (lookAtPosition.y > Y_MAX_LOOKAT_RESET) {
				resetCamera(true);
			}
		}
	}

	private void resetCamera(boolean resetRenderer) {

		initialiseCameraPositionAndVelocity();
		initialiseLookAtPositionAndVelocity();
		initialisePerspectiveCamera();

		if(resetRenderer == true) {
			triangleRenderer.resetCamera(cam);
		}
	}

	private void initialisePerspectiveCamera() {

	}


	private void updateCameraAndLookAtPosition(float dt) {
		cameraVelocity.rotate(rotationAxis, cameraSteerAngularVelocity * dt);
		cameraPosition.add(cameraVelocity.x * dt, cameraVelocity.y * dt, cameraVelocity.z * dt);

		lookAtVelocity.rotate(rotationAxis, lookAtSteerAngularVelocity * dt);

		lookAtPosition.add(lookAtVelocity.x * dt, lookAtVelocity.y * dt, lookAtVelocity.z * dt);
	}

    private void randomiseLookAtVelocity() {

    	int sign;

		if(rnd.nextBoolean() == true) {
			sign = 1;
		} else {
			sign = -1;
		}

		lookAtSteerAngularVelocity = rnd.nextFloat() * MAX_LOOKAT_STEER_ANGULAR_VELOCITY * sign;

		if(lookAtSteerAngularVelocity > MAX_LOOKAT_STEER_ANGULAR_VELOCITY) {
			lookAtSteerAngularVelocity = MAX_LOOKAT_STEER_ANGULAR_VELOCITY;
		}

		else if(lookAtSteerAngularVelocity < -MAX_LOOKAT_STEER_ANGULAR_VELOCITY) {
			lookAtSteerAngularVelocity = -MAX_LOOKAT_STEER_ANGULAR_VELOCITY;
		}

    }


	@Override public void resize(int width, int height) {
	}

	@Override
	public void dispose () {
	}


}
