package com.tuxware.mesh;

import com.badlogic.gdx.math.Vector3;

import java.util.ArrayList;

/**
 * Multi-layer hexagon / honeycomb pattern like KDE wallpaper has.
 */
//TODO: must implement some type of culling for faces, both for backward facing ones, and ones nowhere near on screen. very battery intensive at the moment.
public class CubesFloatingScheme implements MeshScheme {

    private static KirraArrayList<Triangle> triangles = TriangleListSingleton.triangles;

    private static final int NUMBER_OF_DISC_LAYERS = 50;    // independent variable
    private static final float Z_DISC_HEIGHT = 1000;
    private static final float Z_COORDINATE_BASE    = 500.0f + Z_DISC_HEIGHT * NUMBER_OF_DISC_LAYERS;   // dependent variable
    private static int SIDE_LENGTH = 1000;


    @Override
    public KirraArrayList<Triangle> create(float filled, Colorer colorer, TileGenerator tileGenerator, float margin) {

        triangles.clear();

        for(int row = -26; row < 26; row++) {
            for (int column = -26; column < 26; column++) {

                float xcentre = SIDE_LENGTH * column;
                float ycentre = SIDE_LENGTH * row;

                for(int currentDiscHeight = 0; currentDiscHeight < NUMBER_OF_DISC_LAYERS; currentDiscHeight++) {

                    if(MeshWallpaper.rnd.nextFloat() < 0.05f) {
                        generateCube(xcentre, ycentre, currentDiscHeight, colorer, filled, column, row);
                    }
                }
            }
        }

        return triangles;
    }

    private void generateCube(float xcentre, float ycentre, int discHeight, Colorer colorer, float filled, int column, int row) {

        // grow towards camera as discHeight increases
        float baseZ = Z_COORDINATE_BASE - Z_DISC_HEIGHT * discHeight;
        float topZ = baseZ - Z_DISC_HEIGHT;

        float margin = 5;

        // base vertices
        Vector3 baseTopLeft     = new Vector3(-SIDE_LENGTH / 2 + xcentre + margin,  SIDE_LENGTH / 2 + ycentre - margin, baseZ);
        Vector3 baseTopRight    = new Vector3( SIDE_LENGTH / 2 + xcentre - margin,  SIDE_LENGTH / 2 + ycentre - margin, baseZ);
        Vector3 baseBottomLeft  = new Vector3(-SIDE_LENGTH / 2 + xcentre + margin, -SIDE_LENGTH / 2 + ycentre + margin, baseZ);
        Vector3 baseBottomRight = new Vector3( SIDE_LENGTH / 2 + xcentre - margin, -SIDE_LENGTH / 2 + ycentre + margin, baseZ);

        // top vertices
        Vector3 roofTopLeft     = baseTopLeft.cpy();        roofTopLeft.z = topZ;
        Vector3 roofTopRight    = baseTopRight.cpy();       roofTopRight.z = topZ;
        Vector3 roofBottomLeft  = baseBottomLeft.cpy();     roofBottomLeft.z = topZ;
        Vector3 roofBottomRight = baseBottomRight.cpy();    roofBottomRight.z = topZ;

        float[] colorComponents;

        if(MeshWallpaper.rnd.nextFloat() < filled) {
            colorComponents = colorer.getColor(column, row);
        } else {
            colorComponents = Color.NONFILLED;
        }

        com.badlogic.gdx.graphics.Color color = new com.badlogic.gdx.graphics.Color(colorComponents[0], colorComponents[1], colorComponents[2], 1f);

        Vector3 roofCentre = new Vector3(xcentre, ycentre, topZ);

        Triangle triangle;

        // top side
        triangle = new Triangle(baseTopLeft.cpy(), baseTopRight.cpy(), roofTopLeft.cpy(), color, baseTopLeft.cpy().sub(roofCentre));
        triangles.add(triangle);

        triangle = new Triangle(baseTopRight.cpy(), roofTopRight.cpy(), roofTopLeft.cpy(), color, baseTopRight.cpy().sub(roofCentre));
        triangles.add(triangle);

        // right side
        triangle = new Triangle(baseTopRight.cpy(), baseBottomRight.cpy(), roofTopRight.cpy(), color, baseTopRight.cpy().sub(roofCentre));
        triangles.add(triangle);

        triangle = new Triangle(baseBottomRight.cpy(), roofBottomRight.cpy(), roofTopRight.cpy(), color, baseBottomRight.cpy().sub(roofCentre));
        triangles.add(triangle);

        // bottom side
        triangle = new Triangle(baseBottomRight.cpy(), baseBottomLeft.cpy(), roofBottomRight.cpy(), color, baseBottomRight.cpy().sub(roofCentre));
        triangles.add(triangle);

        triangle = new Triangle(baseBottomLeft.cpy(), roofBottomLeft.cpy(), roofBottomRight.cpy(), color, baseBottomLeft.cpy().sub(roofCentre));
        triangles.add(triangle);

        // left side
        triangle = new Triangle(baseBottomLeft.cpy(), baseTopLeft.cpy(), roofBottomLeft.cpy(), color, baseBottomLeft.cpy().sub(roofCentre));
        triangles.add(triangle);

        triangle = new Triangle(baseTopLeft.cpy(), roofTopLeft.cpy(), roofBottomLeft.cpy(), color, baseTopLeft.cpy().sub(roofCentre));
        triangles.add(triangle);

        // roof
        triangle = new Triangle(roofTopLeft.cpy(), roofTopRight.cpy(), roofBottomLeft.cpy(), color);
        triangles.add(triangle);

        triangle = new Triangle(roofTopRight.cpy(), roofBottomRight.cpy(), roofBottomLeft.cpy(), color);
        triangles.add(triangle);
    }

    @Override
    public KirraArrayList<Triangle> update(float x, float y) {

        return null;
    }
}
