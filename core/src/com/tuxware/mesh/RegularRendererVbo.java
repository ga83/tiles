package com.tuxware.mesh;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.PerspectiveCamera;
import com.badlogic.gdx.graphics.VertexAttribute;
import com.badlogic.gdx.graphics.VertexAttributes;
import com.badlogic.gdx.graphics.glutils.ShaderProgram;
import com.badlogic.gdx.graphics.glutils.VertexBufferObject;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.GdxRuntimeException;

import java.util.Arrays;

public class RegularRendererVbo extends TriangleRenderer {

    private static final int NUMBER_OF_POSITION_COMPONENTS = 3;	// xyz
    private static final int NUMBER_OF_COLOR_COMPONENTS = 3;		// rgb
    private static final int NUMBER_OF_NORMAL_COMPONENTS = 3;	// xyz
    private static final int NUMBER_OF_VERTEX_COMPONENTS = NUMBER_OF_POSITION_COMPONENTS + NUMBER_OF_COLOR_COMPONENTS + NUMBER_OF_NORMAL_COMPONENTS;


    private final VertexAttributes vAs;
    protected static float[] verts = new float[18398016]; // #12837942 -// TODO: buffer too big for Android to allocate. maybe make multiple smaller vbos.
                                                                        // TODO: even one per tile? possible overkill. several tiles per vbo is probably best.
    protected static int index = 0;
    private VertexBufferObject vbo;
    private int vertsLength;



    public void render(KirraArrayList<Triangle> triangles) {

        Gdx.gl.glClear(GL20.GL_DEPTH_BUFFER_BIT | GL20.GL_COLOR_BUFFER_BIT);

        shaderProgram.begin();

        // set position to camera
        lightPos[0] = cam.position.x;
        lightPos[1] = cam.position.y;
        lightPos[2] = cam.position.z;
        shaderProgram.setUniform3fv(LightPosLocation,lightPos,0,3);

        cameraPos[0] = cam.position.x;
        cameraPos[1] = cam.position.y;
        cameraPos[2] = cam.position.z;
        shaderProgram.setUniform3fv(CameraPosWorldSpaceLocation, cameraPos, 0, 3);

        shaderProgram.setUniformMatrix(u_ProjTransLocation, cam.combined);

//        Gdx.gl.glDrawArrays(GL20.GL_TRIANGLES, 0, vbo.getNumVertices());
        Gdx.gl.glDrawArrays(GL20.GL_TRIANGLES, 0, this.vertsLength);

        shaderProgram.end();
    }


    protected void addTriangleToVbo(Triangle triangle) {

        Vector3 triangleNormal = triangle.n1;

        float r = triangle.color.r;
        float g = triangle.color.g;
        float b = triangle.color.b;

        //bottom left vertex
        verts[index++] = triangle.p1.x; 			//Position(x, y)
        verts[index++] = triangle.p1.y;
        verts[index++] = triangle.p1.z;

        verts[index++] = r; 	//Color(r, g, b, a)
        verts[index++] = g;
        verts[index++] = b;

        verts[index++] = triangleNormal.x;
        verts[index++] = triangleNormal.y;
        verts[index++] = triangleNormal.z;

        //top left vertex
        verts[index++] = triangle.p2.x; 			//Position(x, y)
        verts[index++] = triangle.p2.y;
        verts[index++] = triangle.p2.z;

        verts[index++] = r; 	//Color(r, g, b, a)
        verts[index++] = g;
        verts[index++] = b;

        verts[index++] = triangleNormal.x;
        verts[index++] = triangleNormal.y;
        verts[index++] = triangleNormal.z;

        //bottom right vertex
        verts[index++] = triangle.p3.x;	 //Position(x, y)
        verts[index++] = triangle.p3.y;
        verts[index++] = triangle.p3.z;

        verts[index++] = r; 	//Color(r, g, b, a)
        verts[index++] = g;
        verts[index++] = b;

        verts[index++] = triangleNormal.x;
        verts[index++] = triangleNormal.y;
        verts[index++] = triangleNormal.z;
    }

    @Override
    public void finalize() {
        vbo.dispose();
        shaderProgram.dispose();
    }


    public RegularRendererVbo(PerspectiveCamera cam, KirraArrayList<Triangle> triangles, String material, String shape) {

        System.out.println("aaa verts size: " + (triangles.size() * 3 * NUMBER_OF_VERTEX_COMPONENTS));

//        verts = new float[triangles.size() * 3 * NUMBER_OF_VERTEX_COMPONENTS];

        int vertsLength = triangles.size() * 3 * NUMBER_OF_VERTEX_COMPONENTS;

        Arrays.fill(verts,0.0f);
        index = 0;

        for (int i = 0; i < triangles.size(); i++) {
            Triangle t1 = triangles.get(i);
            addTriangleToVbo(t1);
        }

        this.cam = cam;

        VertexAttribute vA = new VertexAttribute( VertexAttributes.Usage.Position, 3, "a_position" );
        VertexAttribute vC = new VertexAttribute( VertexAttributes.Usage.Position, 3, "a_color" );
        VertexAttribute vN = new VertexAttribute( VertexAttributes.Usage.Position, 3, "a_normal" );

        this.vAs = new VertexAttributes( new VertexAttribute[]{ vA, vC, vN } );
        this.vbo = new VertexBufferObject(false, verts.length / NUMBER_OF_VERTEX_COMPONENTS, this.vAs);

        String vertexShaderString = Gdx.files.internal("vertex-regular.glsl").readString();
        String fragmentShaderString = null;

        // aside from having selected plastic material, if the mesh shape is one of several, force use plastic because metal looks bad on them
        if(
                material.equals("plastic") == true  ||
                shape.equals("hexagondisc") == true ||
                shape.equals("squaredisc") == true  ||
                shape.equals("cubes") == true       ||
                shape.equals("cubesfloating") == true
        ) {
            fragmentShaderString = Gdx.files.internal("fragment-regular-plastic.glsl").readString();
        }
        else if(material.equals("metal") == true) {
            fragmentShaderString = Gdx.files.internal("fragment-regular-metal.glsl").readString();
        }

        float distance = -cam.position.z;
        fragmentShaderString = fragmentShaderString.replaceAll("__AMPLITUDE__FACTOR__", String.valueOf(3000.0 * Math.pow(distance, 1.05)) );


        ShaderProgram.pedantic = false;
        shaderProgram = new ShaderProgram(vertexShaderString,fragmentShaderString);

        this.vbo.bind(shaderProgram);

        // send the vertices to be stored on the gpu. #12837942
        this.vbo.setVertices(verts, 0, vertsLength);

        this.vertsLength = vertsLength;

        String log = shaderProgram.getLog();
        if (shaderProgram.isCompiled() == false) {
            System.out.println("Shader Log: " + log);
            throw new GdxRuntimeException(log);
        }
        System.out.println("Shader Log: " + log);

        this.getUniformLocations();
    }



}





