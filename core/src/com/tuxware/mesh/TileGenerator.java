package com.tuxware.mesh;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Vector3;

import java.util.ArrayList;


enum RotateExtra {
    NONE,
    HALF,
    PLUS_NINETY
}

public abstract class TileGenerator {

    public abstract KirraArrayList<Triangle> generateTile(float radius, Vector3 centre, int numSides, Color color, RotateExtra rotateExtra, float margin);

}
