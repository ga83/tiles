package com.tuxware.mesh;

import com.badlogic.gdx.math.Vector3;

import java.util.ArrayList;

/**
 * This is more like a "honeycomb" with flattened hexagons meant to be viewed at high zoom level, with say, 10 or less hexagons visible at once. And no elevation differences.
 */
public class OctagonGemScheme implements MeshScheme {

    private static KirraArrayList<Triangle> triangles = TriangleListSingleton.triangles;

    private static final float Z_COORDINATE_BASE = 1070.0f;
    private static final float Z_COORDINATE_TOP = 1000.0f;

    private static final float OCTAGON_SIDE_LENGTH = 1000.0f / 2.0f;
    private static final float OCTAGON_CIRCUMCIRCLE_RADIUS = 1306.6f / 2.0f;     // from the centre to one of the vertices
    private static final float OCTAGON_TOTAL_WIDTH_OR_HEIGHT = (float)(OCTAGON_SIDE_LENGTH + 2 * Math.cos(Math.toRadians(45)) * OCTAGON_SIDE_LENGTH);

    private static final float TOP_LENGTH_PERCENTAGE = 0.75f; // the proportion of the base's edge lengths to use for the top's edge lengths


    @Override
    public KirraArrayList<Triangle> create(float filled, Colorer colorer, TileGenerator tileGenerator, float margin) {

        triangles.clear();

        for(int column = -26; column < 26; column++) {
            for (int row = -26; row < 26; row++) {

                float xcentre = column * OCTAGON_TOTAL_WIDTH_OR_HEIGHT;
                float ycentre = row * OCTAGON_TOTAL_WIDTH_OR_HEIGHT;

                generateOctagonGem(xcentre, ycentre, colorer, filled, column, row, tileGenerator, margin);

                float diamondXCentre = xcentre + OCTAGON_TOTAL_WIDTH_OR_HEIGHT / 2.0f;
                float diamondYCentre = ycentre + OCTAGON_TOTAL_WIDTH_OR_HEIGHT / 2.0f;

                generateFillingDiamond(diamondXCentre, diamondYCentre, colorer, filled, column, row, tileGenerator, margin);
            }
        }


        return triangles;
    }

    private void generateFillingDiamond(float xcentre, float ycentre, Colorer colorer, float filled, int column, int row, TileGenerator tileGenerator, float margin) {
        // base vertices
        Vector3 baseTopLeft        = new Vector3(-OCTAGON_SIDE_LENGTH / 2, OCTAGON_SIDE_LENGTH / 2, Z_COORDINATE_BASE);
        Vector3 baseBottomRight    = new Vector3(OCTAGON_SIDE_LENGTH / 2, -OCTAGON_SIDE_LENGTH / 2, Z_COORDINATE_BASE);

        float[] colorComponents;

        if(MeshWallpaper.rnd.nextFloat() < filled) {
            colorComponents = colorer.getColor(column, row);
        } else {
            colorComponents = Color.NONFILLED;
        }

        float radius = baseBottomRight.dst(baseTopLeft) / 2.0f;
        Vector3 centre = new Vector3(xcentre, ycentre, 0);
        com.badlogic.gdx.graphics.Color color = new com.badlogic.gdx.graphics.Color(colorComponents[0], colorComponents[1], colorComponents[2], 1f);
        KirraArrayList<Triangle> triangles = tileGenerator.generateTile(radius, centre, 4, color, RotateExtra.NONE, margin);
        this.triangles.addAll(triangles);

    }


    private void generateOctagonGem(float xcentre, float ycentre, Colorer colorer, float filled, int column, int row, TileGenerator tileGenerator, float margin) {

        float[] colorComponents;

        if(MeshWallpaper.rnd.nextFloat() < filled) {
            colorComponents = colorer.getColor(column, row);
        } else {
            colorComponents = Color.NONFILLED;
        }

        float radius = OCTAGON_CIRCUMCIRCLE_RADIUS;
        Vector3 centre = new Vector3(xcentre, ycentre, 0);
        com.badlogic.gdx.graphics.Color color = new com.badlogic.gdx.graphics.Color(colorComponents[0], colorComponents[1], colorComponents[2], 1f);
        KirraArrayList<Triangle> triangles = tileGenerator.generateTile(radius, centre, 8, color, RotateExtra.HALF, margin);
        this.triangles.addAll(triangles);
    }

    @Override
    public KirraArrayList<Triangle> update(float x, float y) {
        return null;
    }
}
