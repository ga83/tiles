package com.tuxware.mesh;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.glutils.VertexBufferObject;

import java.util.ArrayList;

public class VBOData {

    public VertexBufferObject vbo;
    public float[] vboVerts;
    public int vboIndex;
    public KirraArrayList<Triangle> triangleList;       // this is assumed to be filled with top and botton triangles interleaved. hence, an even number index is always top, and odd is always bottom.
    public Texture texture;    // although triangles can hold textures, every triangle in this whole vbo uses the same one.


    // for textured triangles
    public VBOData(KirraArrayList<Triangle> triangles, float[] vboVerts, VertexBufferObject vbo, Texture texture) {
        this.vbo = vbo;
        this.vboVerts = vboVerts;
        this.vboIndex = 0;
        this.triangleList = triangles;
        this.texture = texture;
    }

    // for untextued triangles
    public VBOData(KirraArrayList<Triangle> triangles, float[] vboVerts, VertexBufferObject vbo) {
        this.vbo = vbo;
        this.vboVerts = vboVerts;
        this.vboIndex = 0;
        this.triangleList = triangles;
    }

}
