package com.tuxware.mesh;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashSet;



public class KirraArrayList<T> {

    private Object[] backingArray;
    private int endIndex;



    public KirraArrayList(HashSet<T> hashSet, int finalSize) {
        this.backingArray = (T[])(new Object[finalSize]);
        this.addAll(hashSet);
    }


    public KirraArrayList(int finalSize) {
        this.backingArray = (T[])(new Object[finalSize]);
    }


    public HashSet<T> asSet() {
        HashSet<T> hashSet = new HashSet<T>();

        for(int i = 0; i < this.endIndex; i++) {
            hashSet.add((T)this.backingArray[i]);
        }

        return hashSet;
    }


    public void addAll(HashSet<T> hashSet) {
        T[] tArray = (T[])(new ArrayList<T>(hashSet)).toArray();
        int numToCopy = tArray.length;
        System.arraycopy(tArray, 0, this.backingArray, this.endIndex, numToCopy);
        this.endIndex += numToCopy;
    }


    public void addAll(KirraArrayList<T> kirraArrayList) {
        int numToCopy = kirraArrayList.endIndex;
        System.arraycopy(kirraArrayList.backingArray, 0, this.backingArray, this.endIndex, numToCopy);
        this.endIndex += numToCopy;
    }


    public void clear() {
        System.arraycopy(this.backingArray, 0, this.backingArray, 0, this.endIndex);
        this.endIndex = 0;
    }


    public void remove(T item) {
        for(int i = 0; i < this.endIndex; i++) {
            if(this.backingArray[i] == item) {
                this.remove(i);
            }
        }
    }

    public void remove(int index) {
        int numToCopy = this.endIndex - index - 1;

        if (numToCopy > 0) {
            System.arraycopy(this.backingArray, index + 1, this.backingArray, index, numToCopy);
        }
        this.backingArray[--this.endIndex] = null;
    }


    public void add(T item) {
        this.backingArray[this.endIndex] = item;
        this.endIndex ++;
    }


    public T get(int i) {
        return (T)this.backingArray[i];
    }


    public int size() {
        return this.endIndex;
    }


    public void set(int i, T item) {
        this.backingArray[i] = item;
    }


    public void sort(Comparator<T> comparator) {
        Arrays.sort((T[])this.backingArray, 0, this.endIndex, comparator);
    }
}
