package com.tuxware.mesh;

import com.badlogic.gdx.math.Vector3;

import java.util.ArrayList;


/**
 * This is more like a landscape in a game. Fairly fine-grained, rolling hills, etc. Meant to be viewed fairly zoomed out. CONTRAST to HexagonsCliffFaceScheme.
 * NOT MEANT TO BE A VERTICALLY ALIGNED MESH. SHOULD BE CHANGED TO HORIZONTAL, AND THE CAMERA MUST BE ADJUSTED FOR THIS TOO. BECAUSE Y AXIS IS ALWAYS UP.
 */
public class HexagonsLandscapeScheme implements MeshScheme {

    public static final int PERLIN_VALUE_MULTIPLIER = 9000;
    public static final int PERLIN_PERIOD = 12000;
    private static KirraArrayList<Triangle> triangles = TriangleListSingleton.triangles;

    private static final float Z_COORDINATE_BASE = 10000.0f;

    private static final float HEXAGON_CIRCUMCIRCLE_RADIUS_OR_SIDE_LENGTH = 1000.0f / 1;     // from the centre to one of the vertices
    private static final float HEXAGON_INCIRCLE_RADIUS = 866.0f / 1; // not right, just guessing

    @Override
    public KirraArrayList<Triangle> create(float filled, Colorer colorer, TileGenerator tileGenerator, float margin) {

        triangles.clear();

        for(int i = -26; i < 26; i++) {
            for (int j = -26; j < 26; j++) {

                float widthTakenPerHexagon = (float) ((HEXAGON_CIRCUMCIRCLE_RADIUS_OR_SIDE_LENGTH * 2) - Math.cos(Math.toRadians(60.0)) * HEXAGON_CIRCUMCIRCLE_RADIUS_OR_SIDE_LENGTH);

                float verticalPadding = i % 2 == 0 ? HEXAGON_INCIRCLE_RADIUS: 0;
                float xcentre = i * widthTakenPerHexagon;
                float ycentre = j * HEXAGON_INCIRCLE_RADIUS * 2 + verticalPadding;

                generateHexagon(xcentre, ycentre, colorer, filled);
            }
        }
        return triangles;
    }

    private void generateHexagon(float xcentre, float ycentre, Colorer colorer, float filled) {

        // TODO: let the user choose the level of left-out objects
        if(MeshWallpaper.rnd.nextFloat() < 0.5f) {
            //         return;
        }


        for(int i = 0; i < 6; i++) {

            float[] colorComponents;

            colorComponents = colorer.getColor(xcentre, ycentre);

            com.badlogic.gdx.graphics.Color color = new com.badlogic.gdx.graphics.Color(colorComponents[0], colorComponents[1], colorComponents[2], 1f);

            double leftArmAngle  = (360.0 / 6) * i;
            double rightArmAngle = (360.0 / 6) * (i + 1);

            double leftArmX = Math.cos(Math.toRadians(leftArmAngle)) * HEXAGON_CIRCUMCIRCLE_RADIUS_OR_SIDE_LENGTH;
            double leftArmY = Math.sin(Math.toRadians(leftArmAngle)) * HEXAGON_CIRCUMCIRCLE_RADIUS_OR_SIDE_LENGTH;

            double rightArmX = Math.cos(Math.toRadians(rightArmAngle)) * HEXAGON_CIRCUMCIRCLE_RADIUS_OR_SIDE_LENGTH;
            double rightArmY = Math.sin(Math.toRadians(rightArmAngle)) * HEXAGON_CIRCUMCIRCLE_RADIUS_OR_SIDE_LENGTH;

            // TODO: let the user choose the level of deformation maybe. right now, we're using a hardcoded value for PERLIN_VALUE_MULTIPLIER. user might not even want any noise,
            // TODO: and instead only want the centre point closer to the camera, for example.
            // TODO: if the light source is moving around, it could look very good with perlin noise!!! crevases, usually dark, can light up.
            float perlinCentre     = (float) ImprovedNoise.noise(xcentre / PERLIN_PERIOD, ycentre / PERLIN_PERIOD, 0.0) * PERLIN_VALUE_MULTIPLIER;
            float perlinLeftArm    = (float) ImprovedNoise.noise((xcentre + leftArmX) / PERLIN_PERIOD, (ycentre + leftArmY) / PERLIN_PERIOD, 0.0) * PERLIN_VALUE_MULTIPLIER;
            float perlinRightArm   = (float) ImprovedNoise.noise((xcentre + rightArmX) / PERLIN_PERIOD, (ycentre + rightArmY) / PERLIN_PERIOD, 0.0) * PERLIN_VALUE_MULTIPLIER;

            Vector3 centre      = new Vector3(xcentre, ycentre, Z_COORDINATE_BASE + perlinCentre);
            Vector3 leftVertex  = new Vector3(xcentre + (float)leftArmX, ycentre + (float)leftArmY, Z_COORDINATE_BASE + perlinLeftArm);
            Vector3 rightVertex = new Vector3(xcentre + (float)rightArmX, ycentre + (float)rightArmY, Z_COORDINATE_BASE + perlinRightArm);

            Triangle triangle = new Triangle(centre, leftVertex, rightVertex, color);
            triangles.add(triangle);
        }

    }

    @Override
    public KirraArrayList<Triangle> update(float x, float y) {
        return null;
    }
}
