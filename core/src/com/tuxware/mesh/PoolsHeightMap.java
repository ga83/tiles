package com.tuxware.mesh;

import com.badlogic.gdx.math.Vector2;

public class PoolsHeightMap implements HeightMap {

    public static final int NUM_COLORS = 100;
    private static final int MAX_AMPLITUDE = 49;
    private static final int NUM_KNOTS = 2;

    private final int width;
    private final int height;

    public double[][] heightMap;
    HeightMapPoint[] spouts;
    HeightMapPoint[] sinks;


    @Override
    public void generate() {

        int numSpouts;
        int numSinks;

        numSpouts = MeshWallpaper.rnd.nextInt(NUM_KNOTS);
        numSinks = NUM_KNOTS - numSpouts;

        spouts  = new HeightMapPoint[numSpouts];
        sinks   = new HeightMapPoint[numSinks];

        // spouts
        for(int i = 0; i < numSpouts; i++) {
            int x = MeshWallpaper.rnd.nextInt(width);
            int y = MeshWallpaper.rnd.nextInt(height);
            float amplitude = i == 0 ? MAX_AMPLITUDE / 2 : MeshWallpaper.rnd.nextInt(MAX_AMPLITUDE);
            spouts[i] = new HeightMapPoint(x, y, amplitude);
        }

        // sinks
        for(int i = 0; i < numSinks; i++) {
            int x = MeshWallpaper.rnd.nextInt(width);
            int y = MeshWallpaper.rnd.nextInt(height);
            float amplitude = i == 0 ? -MAX_AMPLITUDE / 2 : -MeshWallpaper.rnd.nextInt(MAX_AMPLITUDE);
            sinks[i] = new HeightMapPoint(x, y, amplitude);
        }

        for(int row = 0; row < height; row++) {
            for(int column = 0; column < width; column++) {

                double heightMapValue = 0;

                for(int i = 0; i < numSpouts; i++) {
                    Vector2 pointVector         = new Vector2(spouts[i].x, spouts[i].y);
                    Vector2 currentCellVector   = new Vector2(column, row);

                    float distance = pointVector.dst(currentCellVector);

                    if(distance < 1.0) {
                        distance = 1;
                    }

                    heightMapValue += (1 / distance) * spouts[i].amplitude;
                }

                for(int i = 0; i < numSinks; i++) {
                    Vector2 pointVector         = new Vector2(sinks[i].x, sinks[i].y);
                    Vector2 currentCellVector   = new Vector2(column, row);

                    float distance = pointVector.dst(currentCellVector);

                    if(distance < 1.0) {
                        distance = 1;
                    }

                    heightMapValue -= (1 / distance) * sinks[i].amplitude;
                }

                heightMap[row][column] = heightMapValue;
            }
        }

        double maximum = Double.MIN_VALUE;
        double minimum = Double.MAX_VALUE;

        for(int row = 0; row < height; row++) {
            for (int column = 0; column < width; column++) {

                if(heightMap[row][column] > maximum) {
                    maximum = heightMap[row][column];
                }

                if(heightMap[row][column] < minimum) {
                    minimum = heightMap[row][column];
                }
            }
        }

        double normaliseFactor;
        double maximumDistanceFromZero = maximum - 0;
        double minimumDistanceFromZero = 0 - minimum;

        if(maximumDistanceFromZero > minimumDistanceFromZero) {
            normaliseFactor = maximumDistanceFromZero / ((NUM_COLORS - 1) / 2);
        } else {
            normaliseFactor = minimumDistanceFromZero / ((NUM_COLORS - 1) / 2);
        }

        for(int row = 0; row < height; row++) {
            for (int column = 0; column < width; column++) {
                heightMap[row][column] /= normaliseFactor;
                heightMap[row][column] += NUM_COLORS / 2;
            }
        }
    }


    public double[][] getHeightMap() {
        return this.heightMap;
    }

    public PoolsHeightMap(int width, int height) {
        this.width = width;
        this.height = height;

        heightMap = new double[width][height];
    }

}
