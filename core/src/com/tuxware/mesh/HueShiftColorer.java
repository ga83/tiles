package com.tuxware.mesh;

public class HueShiftColorer extends Colorer {


    @Override
    public float[] getColor(float x, float y) {

        int height = (int) heightMap.getHeightMap()[26 + (int)x][26 + (int)y];

        float[] colorComponents = colorComponentsArray[height];
        return colorComponents;
    }

    public HueShiftColorer(String colorScheme) {
        super(colorScheme);

        heightMap = new HueShiftHeightMap(26 * 2, 26 * 2);
        heightMap.generate();

        float hueStart = Color.getRandomColorFromScheme(colorScheme, true)[0] - 0.15f;
        float hueRange = 0.3f;

        for(int i = 0; i < HueShiftHeightMap.NUM_COLORS; i++) {
            float hue = hueStart + (i / (float)HueShiftHeightMap.NUM_COLORS) * hueRange;
            colorComponentsArray[i] = Color.getColorFromHue(hue);
        }

    }
}
