package com.tuxware.mesh;

import com.badlogic.gdx.math.GridPoint2;
import com.badlogic.gdx.math.Vector3;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Random;

public class MotelTilesScheme implements MeshScheme {

    private static final float Z_COORDINATE_BASE    = 1200.0f;
    private static final float Z_COORDINATE_TOP     = 1000.0f;
    private static int SIDE_LENGTH = 1000;

    private static KirraArrayList<Triangle> triangles = TriangleListSingleton.triangles;

    private boolean[][] boardFilled = new boolean[52][52];    // filled: true, not filled: false
    private Random rnd = new Random();
    private KirraArrayList<GridPoint2> emptyCellsList = new KirraArrayList<GridPoint2>(10000);
    private HashSet<GridPoint2> emptyCellsSet = new HashSet<GridPoint2>();



    public MotelTilesScheme() {

        this.generateEmptyCellsSet();
        this.triangles.clear();

        System.out.println("aaa generated empty cells");
    }

    private void generateEmptyCellsSet() {

        for(int x = 0; x < this.boardFilled.length; x++) {
            for(int y = 0; y < this.boardFilled[0].length; y++) {

                GridPoint2 gp2 = new GridPoint2(x, y);
                this.emptyCellsSet.add(gp2);
            }

        }



    }


    @Override
    public KirraArrayList<Triangle> create(float targetFilledFraction, Colorer colorer, TileGenerator tileGenerator, float margin) {

        float currentFilledFraction = 0.0f;

        // filled tiles
        while (currentFilledFraction < targetFilledFraction) {
            this.generateRectangle(colorer, true, margin);
            currentFilledFraction = this.calculateBoardFilled();
        }

        // unfilled tiles
        while (currentFilledFraction < 1.0f) {
            this.generateRectangle(colorer, false, margin);
            currentFilledFraction = this.calculateBoardFilled();
        }

        return triangles;
    }


    private void generateRectangle(Colorer colorer, boolean filled, float margin) {

        System.out.println("aaa generateRectangle 111");

        // pick another coordinate
//        this.emptyCellsList = new KirraArrayList<GridPoint2>(this.emptyCellsSet);
        this.emptyCellsList = new KirraArrayList<GridPoint2>(10000);
        this.emptyCellsList.addAll(this.emptyCellsSet);

        int index = this.rnd.nextInt(this.emptyCellsList.size());

        GridPoint2 gp2 = this.emptyCellsList.get(index);


        GridPoint2[] gridpoints;
        boolean down;
        boolean right;

        // try 2x2... randomly choose vertical and horizontal directions (up or down, left or right)
        down    = this.rnd.nextBoolean();
        right = this.rnd.nextBoolean();
        gridpoints = this.get2x2GridPoints(gp2, down, right);
        if(this.allInRange(gridpoints) == true && this.allEmpty(gridpoints) == true) {
            for(int i = 0; i < gridpoints.length; i++) {
                int x = gridpoints[i].x;
                int y = gridpoints[i].y;
                this.boardFilled[x][y] = true;
                this.emptyCellsSet.remove(gp2);
            }

            float[] x1x2y1y1 = this.getx1x2y1y2(gridpoints);
            this.generateRectangleDisc(x1x2y1y1[0], x1x2y1y1[1], x1x2y1y1[2], x1x2y1y1[3], colorer, filled, gp2.y - 26, gp2.x - 26, margin);
            return;
        }

        // try 2x1
        right = this.rnd.nextBoolean();
        gridpoints = this.get2x1GridPoints(gp2, right);
        if(this.allInRange(gridpoints) == true && this.allEmpty(gridpoints) == true) {
            for (int i = 0; i < gridpoints.length; i++) {
                int x = gridpoints[i].x;
                int y = gridpoints[i].y;
                this.boardFilled[x][y] = true;
                this.emptyCellsSet.remove(gp2);
            }

            float[] x1x2y1y1 = this.getx1x2y1y2(gridpoints);
            this.generateRectangleDisc(x1x2y1y1[0], x1x2y1y1[1], x1x2y1y1[2], x1x2y1y1[3], colorer, filled, gp2.y - 26, gp2.x - 26, margin);
            return;
        }

        // try 1x2
        down = this.rnd.nextBoolean();
        gridpoints = this.get1x2GridPoints(gp2, down);
        if(this.allInRange(gridpoints) == true && this.allEmpty(gridpoints) == true) {
            for (int i = 0; i < gridpoints.length; i++) {
                int x = gridpoints[i].x;
                int y = gridpoints[i].y;
                this.boardFilled[x][y] = true;
                this.emptyCellsSet.remove(gp2);
            }

            float[] x1x2y1y1 = this.getx1x2y1y2(gridpoints);
            this.generateRectangleDisc(x1x2y1y1[0], x1x2y1y1[1], x1x2y1y1[2], x1x2y1y1[3], colorer, filled, gp2.y - 26, gp2.x - 26, margin);
            return;
        }



        // fail, try 1x1
        gridpoints = new GridPoint2[] { gp2 };
        if(this.allInRange(gridpoints) == true && this.allEmpty(gridpoints) == true) {
            for (int i = 0; i < gridpoints.length; i++) {
                int x = gridpoints[0].x;
                int y = gridpoints[0].y;
                this.boardFilled[x][y] = true;
                this.emptyCellsSet.remove(gp2);
            }
            float[] x1x2y1y1 = this.getx1x2y1y2(gridpoints);
            this.generateRectangleDisc(x1x2y1y1[0], x1x2y1y1[1], x1x2y1y1[2], x1x2y1y1[3], colorer, filled, gp2.y - 26, gp2.x - 26, margin);
            return;
        }
    }



    private void generateRectangleDisc(float x1, float x2, float y1, float y2, Colorer colorer, boolean filled, int column, int row, float margin) {

        float CONST = 26 * SIDE_LENGTH;

        // base vertices
        Vector3 baseTopLeft     = new Vector3(x1 - CONST + margin, y1 - CONST + margin, Z_COORDINATE_BASE);
        Vector3 baseTopRight    = new Vector3(x2 - CONST - margin, y1 - CONST + margin, Z_COORDINATE_BASE);
        Vector3 baseBottomLeft  = new Vector3(x1 - CONST + margin, y2 - CONST - margin, Z_COORDINATE_BASE);
        Vector3 baseBottomRight = new Vector3(x2 - CONST - margin, y2 - CONST - margin, Z_COORDINATE_BASE);

        // top vertices
        Vector3 roofTopLeft     = new Vector3(x1 - CONST + margin, y1 - CONST + margin, Z_COORDINATE_TOP);
        Vector3 roofTopRight    = new Vector3(x2 - CONST - margin, y1 - CONST + margin, Z_COORDINATE_TOP);
        Vector3 roofBottomLeft  = new Vector3(x1 - CONST + margin, y2 - CONST - margin, Z_COORDINATE_TOP);
        Vector3 roofBottomRight = new Vector3(x2 - CONST - margin, y2 - CONST - margin, Z_COORDINATE_TOP);

        float[] colorComponents;

        if(filled == true) {
            colorComponents = colorer.getColor(column, row);
        } else {
            colorComponents = Color.NONFILLED;
        }

        com.badlogic.gdx.graphics.Color color = new com.badlogic.gdx.graphics.Color(colorComponents[0], colorComponents[1], colorComponents[2], 1f);

        Vector3 roofCentre = new Vector3((x2 + x1 - CONST - CONST) / 2.0f, (y2 + y1 - CONST - CONST) / 2.0f, Z_COORDINATE_TOP);

        Vector3 centre = new Vector3((x2 + x1 - CONST - CONST) / 2.0f, (y2 + y1 - CONST - CONST) / 2.0f, (Z_COORDINATE_BASE + Z_COORDINATE_TOP) / 2);



        Triangle triangle;

        // top side
        triangle = new Triangle(baseTopLeft.cpy(), baseTopRight.cpy(), roofTopLeft.cpy(), color, baseTopLeft.cpy().sub(centre).nor());
        triangles.add(triangle);

        triangle = new Triangle(baseTopRight.cpy(), roofTopRight.cpy(), roofTopLeft.cpy(), color, baseTopRight.cpy().sub(centre).nor());
        triangles.add(triangle);


        // right side
        triangle = new Triangle(baseTopRight.cpy(), baseBottomRight.cpy(), roofTopRight.cpy(), color, baseTopRight.cpy().sub(centre).nor());
        triangles.add(triangle);

        triangle = new Triangle(baseBottomRight.cpy(), roofBottomRight.cpy(), roofTopRight.cpy(), color, baseBottomRight.cpy().sub(centre).nor());
        triangles.add(triangle);


        // bottom side
        triangle = new Triangle(baseBottomRight.cpy(), baseBottomLeft.cpy(), roofBottomRight.cpy(), color, baseBottomRight.cpy().sub(centre).nor());
        triangles.add(triangle);

        triangle = new Triangle(baseBottomLeft.cpy(), roofBottomLeft.cpy(), roofBottomRight.cpy(), color, baseBottomLeft.cpy().sub(centre).nor());
        triangles.add(triangle);


        // left side
        triangle = new Triangle(baseBottomLeft.cpy(), baseTopLeft.cpy(), roofBottomLeft.cpy(), color, baseBottomLeft.cpy().sub(centre).nor());
        triangles.add(triangle);

        triangle = new Triangle(baseTopLeft.cpy(), roofTopLeft.cpy(), roofBottomLeft.cpy(), color, baseTopLeft.cpy().sub(centre).nor());
        triangles.add(triangle);


        // roof
        triangle = new Triangle(roofTopLeft.cpy(), roofTopRight.cpy(), roofBottomLeft.cpy(), color, roofCentre.cpy().sub(centre).nor());
        triangles.add(triangle);

        triangle = new Triangle(roofTopRight.cpy(), roofBottomRight.cpy(), roofBottomLeft.cpy(), color, roofCentre.cpy().sub(centre).nor());
        triangles.add(triangle);

    }



    private float[] getx1x2y1y2(GridPoint2[] gridpoints) {
        float[] coords;

        float x1 = gridpoints[0].x * SIDE_LENGTH;
        float x2 = gridpoints[gridpoints.length - 1].x * SIDE_LENGTH + SIDE_LENGTH;
        float y1 = gridpoints[0].y * SIDE_LENGTH;
        float y2 = gridpoints[gridpoints.length - 1].y * SIDE_LENGTH + SIDE_LENGTH;

        coords = new float[] { x1, x2, y1, y2 };

        return coords;
    }


    private boolean allEmpty(GridPoint2[] gridpoints) {
        for(int i = 0; i < gridpoints.length; i++) {
            GridPoint2 gp2 = gridpoints[i];

            if(this.boardFilled[gp2.x][gp2.y] == true) {
                return false;
            }
        }

        return true;
    }

    private boolean allInRange(GridPoint2[] gridpoints) {
        if(gridpoints[0].x < 0 || gridpoints[gridpoints.length - 1].x > this.boardFilled.length - 1) {
            return false;
        }

        if(gridpoints[0].y < 0 || gridpoints[gridpoints.length - 1].y > this.boardFilled[0].length - 1) {
            return false;
        }

        return true;
    }


    private GridPoint2[] get1x2GridPoints(GridPoint2 gp2, boolean down) {

        // [0]
        // [1]

        GridPoint2[] gridpoints = new GridPoint2[2];

        if (down == true) {
            gridpoints[0] = gp2.cpy();
            gridpoints[1] = new GridPoint2(gp2.x, gp2.y + 1);
        } else {
            gridpoints[0] = new GridPoint2(gp2.x, gp2.y - 1);
            gridpoints[1] = gp2.cpy();
        }

        return gridpoints;
    }


    private GridPoint2[] get2x1GridPoints(GridPoint2 gp2, boolean right) {

        // [0][1]

        GridPoint2[] gridpoints = new GridPoint2[2];

        if (right == true) {
            gridpoints[0] = gp2.cpy();
            gridpoints[1] = new GridPoint2(gp2.x + 1, gp2.y);
        } else {
            gridpoints[0] = new GridPoint2(gp2.x - 1, gp2.y);
            gridpoints[1] = gp2.cpy();
        }

        return gridpoints;
    }


    private GridPoint2[] get2x2GridPoints(GridPoint2 gp2, boolean down, boolean right) {

        // [0][1]
        // [2][3]

        GridPoint2[] gridpoints = new GridPoint2[4];

        if(down == true) {
            if(right == true) {
                gridpoints[0] = gp2.cpy();
                gridpoints[1] = new GridPoint2(gp2.x + 1, gp2.y);
                gridpoints[2] = new GridPoint2(gp2.x, gp2.y + 1);
                gridpoints[3] = new GridPoint2(gp2.x + 1, gp2.y + 1);
            }
            else {
                gridpoints[0] = new GridPoint2(gp2.x - 1, gp2.y);
                gridpoints[1] = gp2.cpy();
                gridpoints[2] = new GridPoint2(gp2.x - 1, gp2.y + 1);
                gridpoints[3] = new GridPoint2(gp2.x, gp2.y + 1);
            }
        }
        else {
            if(right == true) {
                gridpoints[0] = new GridPoint2(gp2.x, gp2.y - 1);
                gridpoints[1] = new GridPoint2(gp2.x + 1, gp2.y - 1);
                gridpoints[2] = gp2.cpy();
                gridpoints[3] = new GridPoint2(gp2.x + 1, gp2.y);
            }
            else {
                gridpoints[0] = new GridPoint2(gp2.x - 1, gp2.y - 1);
                gridpoints[1] = new GridPoint2(gp2.x, gp2.y - 1);
                gridpoints[2] = new GridPoint2(gp2.x - 1, gp2.y);
                gridpoints[3] = gp2.cpy();
            }
        }

        return gridpoints;
    }


    private float calculateBoardFilled() {
        int total = boardFilled.length * boardFilled[0].length;
        int found = 0;

        for(int x = 0; x < this.boardFilled.length; x++) {
            for(int y = 0; y < this.boardFilled[0].length; y++) {
                if(boardFilled[x][y] == true) {
                    found ++;
                }

            }
        }

        return (float)found / (float)total;
    }



    @Override
    public KirraArrayList<Triangle> update(float x, float y) {
        return null;
    }


}
