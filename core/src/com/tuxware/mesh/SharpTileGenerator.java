package com.tuxware.mesh;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Vector3;

import java.util.ArrayList;

public class SharpTileGenerator extends TileGenerator {

    private static final float Z_COORDINATE_BASE    = 1070.0f;


    // each step, the angle decrements by an equal amount. this means that the angle change on each step depends on the number of steps.
    private static float STEPS = 1;
    private static float INITIAL_ANGLE = 30;
    private static float EDGE_LENGTH_PER_STEP_FRACTION = 0.1f;  // the fraction of the length of one standard edge length (eg for 2x1 rectangles, the 1 side)
    private static Vector3 VERTICAL_AXIS = new Vector3(0, 0, 1);



    @Override
    public KirraArrayList<Triangle> generateTile(float radius, Vector3 centre, int numSides, Color color, RotateExtra rotateExtra, float margin) {
        KirraArrayList<Triangle> triangles = new KirraArrayList<Triangle>(100);
        float deltaAngle = (INITIAL_ANGLE - 0) / STEPS;

        float centreToCornerLength = radius;
        centreToCornerLength -= margin;

        float xcentre = centre.x;
        float ycentre = centre.y;


        // outer loop, one iteration for each side.
        for(int i = 0; i < numSides; i++) {
            Vector3 topCentrePrevious = null;
            Vector3 topLeftPrevious = null;
            Vector3 topRightPrevious = null;

            // inner loop, one iteration for each step.
            for(int j = 0; j < STEPS; j++) {
                float currentEdgeAngle = INITIAL_ANGLE - j * deltaAngle;
                double leftArmAngle = 0.0f;
                double rightArmAngle = 0.0f;

                if(rotateExtra == RotateExtra.HALF) {
                    leftArmAngle = ((360.0 / numSides) * i) + (360.0 / numSides / 2);
                    rightArmAngle = (360.0 / numSides) * (i + 1) + (360.0 / numSides / 2);
                } else if (rotateExtra == RotateExtra.NONE){
                    leftArmAngle = ((360.0 / numSides) * i);
                    rightArmAngle = (360.0 / numSides) * (i + 1);
                } else if (rotateExtra == RotateExtra.PLUS_NINETY){
                    leftArmAngle = ((360.0 / numSides) * i) + 90.0f;
                    rightArmAngle = (360.0 / numSides) * (i + 1) + 90.0f;
                }

                Vector3 baseCentre;
                Vector3 baseLeftVertex;
                Vector3 baseRightVertex;

                // base vertices
                if(topCentrePrevious == null) {
                    double baseLeftArmX = Math.cos(Math.toRadians(leftArmAngle)) * centreToCornerLength;
                    double baseLeftArmY = Math.sin(Math.toRadians(leftArmAngle)) * centreToCornerLength;
                    double baseRightArmX = Math.cos(Math.toRadians(rightArmAngle)) * centreToCornerLength;
                    double baseRightArmY = Math.sin(Math.toRadians(rightArmAngle)) * centreToCornerLength;
                    baseCentre      = new Vector3(xcentre, ycentre, Z_COORDINATE_BASE);
                    baseLeftVertex  = new Vector3(xcentre + (float)baseLeftArmX, ycentre + (float)baseLeftArmY, Z_COORDINATE_BASE);
                    baseRightVertex = new Vector3(xcentre + (float)baseRightArmX, ycentre + (float)baseRightArmY, Z_COORDINATE_BASE);
                } else {
                    baseCentre = topCentrePrevious.cpy();
                    baseLeftVertex = topLeftPrevious.cpy();
                    baseRightVertex = topRightPrevious.cpy();
                }


                Vector3 edgeLeftArmDirection = baseCentre.cpy().sub(baseLeftVertex).nor();
                Vector3 edgeRightArmDirection = baseCentre.cpy().sub(baseRightVertex).nor();

                Vector3 leftRotationAxis = edgeLeftArmDirection.cpy().rotate(VERTICAL_AXIS, -90);
                Vector3 rightRotationAxis = edgeRightArmDirection.cpy().rotate(VERTICAL_AXIS, -90);

                edgeLeftArmDirection.rotate(leftRotationAxis, currentEdgeAngle);
                edgeRightArmDirection.rotate(rightRotationAxis, currentEdgeAngle);

                if(edgeLeftArmDirection.z > 0) {
                    edgeLeftArmDirection.z *= -1;
                }
                if(edgeRightArmDirection.z > 0) {
                    edgeRightArmDirection.z *= -1;
                }

                float edgeLength = baseLeftVertex.dst(baseRightVertex);

                // top vertices
                Vector3 topLeftVertex  = baseLeftVertex.cpy().add(edgeLeftArmDirection.cpy().scl(EDGE_LENGTH_PER_STEP_FRACTION * edgeLength));
                Vector3 topRightVertex = baseRightVertex.cpy().add(edgeRightArmDirection.cpy().scl(EDGE_LENGTH_PER_STEP_FRACTION * edgeLength));

                Triangle triangle;

                // wall triangle 1
                triangle = new Triangle(baseLeftVertex.cpy(),topRightVertex.cpy(),topLeftVertex.cpy(), color);
                triangles.add(triangle);

                // wall triangle 2
                triangle = new Triangle(baseRightVertex.cpy(),topRightVertex.cpy(),baseLeftVertex.cpy(), color);
                triangles.add(triangle);

                Vector3 topCentre = baseCentre.cpy();
                topCentre.z = topRightVertex.z;

                if(j == STEPS - 1) {
                    // quadrilateral from top to center
                    Triangle triangletop = new Triangle(topRightVertex.cpy(), topLeftVertex.cpy(), topCentre, color);
                    triangles.add(triangletop);
                }

                topCentrePrevious = topCentre;
                topLeftPrevious = topLeftVertex;
                topRightPrevious = topRightVertex;
            }
        }

        return triangles;
    }
}
