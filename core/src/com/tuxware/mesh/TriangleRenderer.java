package com.tuxware.mesh;

import com.badlogic.gdx.graphics.PerspectiveCamera;
import com.badlogic.gdx.graphics.glutils.ShaderProgram;

import java.util.ArrayList;

public abstract class TriangleRenderer {

    protected static float[] lightPos = new float[] { 0, 0, 0 };	// this seems to be relative to the screen / camera in the texturedTriangleShaderProgram, so the light sources "follow" the screen. not necessarily wwhat we want, but if we want to change it we might have to transform its position in the vertex texturedTriangleShaderProgram maybe?
    protected static float[] cameraPos = new float[] { 0, 0, 0 };



    protected static PerspectiveCamera cam;

    public abstract void render(KirraArrayList<Triangle> triangles);

    public void resetCamera(PerspectiveCamera cam) {
        this.cam = cam;
    }

//    protected abstract void getUniformLocations();

    protected ShaderProgram shaderProgram;

    protected void getUniformLocations() {
        LightPosLocation = shaderProgram.getUniformLocation("LightPos");
        CameraPosWorldSpaceLocation = shaderProgram.getUniformLocation("CameraPosWorldSpace");
        u_ProjTransLocation = shaderProgram.getUniformLocation("u_projTrans");
    }


    protected int LightPosLocation;
    protected int CameraPosWorldSpaceLocation;
    protected int u_ProjTransLocation;




}
