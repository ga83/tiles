package applogger;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Net;
import com.badlogic.gdx.net.HttpRequestBuilder;

/**
 * Created by ubuntu on 5/09/18.
 */

public class MeshWallpaperLogger implements GdxAppUsageLogger  {

    String url = null;
    int interval = -1;
    String applicationName = null;

    @Override
    public void log() throws ParametersNotSetException {

        if(url == null || interval == -1 || applicationName == null) {
            throw new ParametersNotSetException();
        }

        else {
            while(true) {
                HttpRequestBuilder requestBuilder = new HttpRequestBuilder();
                Net.HttpRequest request = requestBuilder.newRequest().method(Net.HttpMethods.GET).url(url + "?application=" + applicationName + "&interval=" + interval).build();
                request.setTimeOut(1000 * 20);

                Gdx.net.sendHttpRequest(request, this);

                try {
                    Thread.sleep(1000 * interval);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @Override
    public void setUrl(String url) {
        this.url = url;
    }

    @Override
    public void setInterval(int interval) {
        this.interval = interval;
    }

    @Override
    public void setApplicationName(String appname) {
        this.applicationName = appname;
    }

    @Override
    public void handleHttpResponse(Net.HttpResponse httpResponse) {
//        String responseString = httpResponse.getResultAsString();
    }

    @Override
    public void failed(Throwable t) {

    }

    @Override
    public void cancelled() {

    }
}
