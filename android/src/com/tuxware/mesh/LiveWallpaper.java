/*******************************************************************************
 * Copyright 2011 See AUTHORS file.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package com.tuxware.mesh;

import android.annotation.TargetApi;
import android.content.SharedPreferences;
import android.content.res.Configuration;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.backends.android.AndroidApplicationConfiguration;
import com.badlogic.gdx.backends.android.AndroidLiveWallpaperService;
import com.badlogic.gdx.backends.android.AndroidWallpaperListener;

import android.os.Build;


public class LiveWallpaper extends AndroidLiveWallpaperService {
	
	@TargetApi(Build.VERSION_CODES.M)
	@Override
	public void onCreateApplication () {
		super.onCreateApplication();
		AndroidApplicationConfiguration config = new AndroidApplicationConfiguration();

		// load aa samples
		SharedPreferences prefs = this.getSharedPreferences("prefs", MODE_PRIVATE);
		int samples = Integer.valueOf(prefs.getString("aasamples", "2"));
		config.numSamples = samples;

		config.depth = 24;

		ApplicationListener listener = new MyLiveWallpaperListener();
		initialize(listener, config);
	}

	@Override
	public void onConfigurationChanged(Configuration config) {
		super.onConfigurationChanged(config);
	}


	// implement AndroidWallpaperListener additionally to ApplicationListener 
	// if you want to receive callbacks specific to live wallpapers
	public static class MyLiveWallpaperListener extends MeshWallpaper implements AndroidWallpaperListener {

		@Override public void resize(int width, int height) {
		}



		@Override
		public void offsetChange (float xOffset, float yOffset, float xOffsetStep, float yOffsetStep, int xPixelOffset,
			int yPixelOffset) {
		}

		@Override
		public void previewStateChange (boolean isPreview) {
		}

		@Override
		public void iconDropped (int x, int y) {
		}
		
		
	}
}