package com.tuxware.mesh;


import android.Manifest;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.preference.Preference;
import android.preference.PreferenceActivity;

public class MeshPreferencesActivity extends PreferenceActivity {

    // hack, because tossing the prefs creation into onCreate had some bugs. so just add prefs in onResume if this flag hasn't been set yet.
    private boolean prefsAdded = false;

    // TODO: maybe try this, especially if the current file picker solution doesn't work with all devices
    // TODO: https://github.com/passy/Android-DirectoryChooser


    Preference.OnPreferenceChangeListener samplesChangedListener = new Preference.OnPreferenceChangeListener() {
        @Override
        public boolean onPreferenceChange(Preference preference, Object newValue) {

            new Handler().postDelayed(new Runnable() {
                                          @Override
                                          public void run() {
                                              MeshWallpaper.restart();
                                          }
                                      },
                    1000);

            return true;
        }
    };

    @Override
    public void onResume() {
        super.onResume();

        if (prefsAdded == false) {
            getPreferenceManager().setSharedPreferencesName("prefs");
            addPreferencesFromResource(R.xml.prefs);


            getPreferenceScreen().findPreference("shape").setOnPreferenceChangeListener(prefsChangedListener);
            getPreferenceScreen().findPreference("colorscheme").setOnPreferenceChangeListener(prefsChangedListener);
            getPreferenceScreen().findPreference("filled").setOnPreferenceChangeListener(prefsChangedListener);
            getPreferenceScreen().findPreference("style").setOnPreferenceChangeListener(prefsChangedListener);
            getPreferenceScreen().findPreference("fov").setOnPreferenceChangeListener(prefsChangedListener);
            getPreferenceScreen().findPreference("cameradistance").setOnPreferenceChangeListener(prefsChangedListener);
            getPreferenceScreen().findPreference("nonfilledcolor").setOnPreferenceChangeListener(prefsChangedListener);
            getPreferenceScreen().findPreference("material").setOnPreferenceChangeListener(prefsChangedListener);
            getPreferenceScreen().findPreference("fps").setOnPreferenceChangeListener(prefsChangedListener);
            getPreferenceScreen().findPreference("cameraspeed").setOnPreferenceChangeListener(prefsChangedListener);
            getPreferenceScreen().findPreference("tiletype").setOnPreferenceChangeListener(prefsChangedListener);
            getPreferenceScreen().findPreference("tilegap").setOnPreferenceChangeListener(prefsChangedListener);

            getPreferenceScreen().findPreference("aasamples").setOnPreferenceChangeListener(samplesChangedListener);


            // only allow after we add predefined (not user selectable) textures
            //getPreferenceScreen().findPreference("texturequality").setOnPreferenceChangeListener(prefsChangedListener);


            getListView().setBackgroundColor(Color.argb(192, 255, 255, 255));

        /*
            Preference filePicker = (Preference) findPreference("filePicker");
            filePicker.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                @Override
                public boolean onPreferenceClick(Preference preference) {

                    Intent i = new Intent(Intent.ACTION_OPEN_DOCUMENT_TREE);
                    i.addCategory(Intent.CATEGORY_DEFAULT);
                    startActivityForResult(Intent.createChooser(i, "Choose image folder"), 69);
                    return true;
                }
            });
            */

        }
        prefsAdded = true;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getPreferenceManager().setSharedPreferencesName("prefs");
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
    }

    private boolean androidVersionHighEnough() {
        return (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1);
    }

    public boolean isExternalStorageReadable() {
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state) ||
                Environment.MEDIA_MOUNTED_READ_ONLY.equals(state)) {
            return true;
        }
        return false;
    }


    Preference.OnPreferenceChangeListener prefsChangedListener = new Preference.OnPreferenceChangeListener() {
        @Override
        public boolean onPreferenceChange(Preference preference, Object newValue) {
            // usually we would check that the selected value is the right type, etc, number, but don't worry about that yet. just return true.
            MeshWallpaper.preferencesChanged = true;
            return true;
        }
    };
}