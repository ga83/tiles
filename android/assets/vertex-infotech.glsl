//our attributes
attribute vec3 a_position;
attribute vec4 a_color;
attribute vec3 a_normal;

//our camera matrix
uniform mat4 u_projTrans;

//send the color out to the fragment shader
varying vec4 vColor;
varying vec3 vNormal;
varying vec3 fragPosition;

// barycentric outline test
varying vec3 vBC;			// output
attribute vec3 barycentric;	// input


void main() {

    vBC = barycentric;

    vColor = a_color;
    vNormal = a_normal;

    gl_Position = u_projTrans * vec4(a_position.xyz, 1.0);


    // HACK TO MAKE SURE THAT OBJECTS APPEAR CORRECT IN TERMS OF X COORDS... NOT REAL SOLUTION. WITHOUT THIS, NEGATIVE X COORDS VERTICES GO TO THE RIGHT OF THE SCREEN...
    gl_Position.x *= -1.0;

    fragPosition = gl_Position.xyz;
}