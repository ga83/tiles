#ifdef GL_ES
precision highp float;
#endif

//attributes from vertex shader
varying vec4 vColor;
varying vec3 vNormal;

//our texture samplers
uniform sampler2D u_texture0;   //diffuse map
uniform sampler2D u_normals;   //normal map

uniform vec3 LightPos;     //light position, normalized
varying vec3 fragPosition;

uniform sampler2D texture;
varying vec2 texCoord;


void main() {

//    vec4 finalColor = vColor;

    /*
    vec4 finalColor = vec4(1.0,1.0,1.0,1.0);

    float Sum = 0.0;
    float diffuseLighting;
    float amplitude = __AMPLITUDE__FACTOR__ / pow ( (distance(LightPos,fragPosition)) , 2.0) * 1.5;


    // ambient lighting
    Sum += 0.15;


    // diffuse lighting
    vec3 backToLightSource = normalize(LightPos - fragPosition);
    diffuseLighting = dot(backToLightSource, normalize(vNormal)) * amplitude * 0.85;
    Sum += diffuseLighting;
*/

  //  gl_FragColor = finalColor * Sum;

    gl_FragColor = texture2D(texture, texCoord);

//    gl_FragColor = texture2D(texture, texCoord) * Sum;
}



