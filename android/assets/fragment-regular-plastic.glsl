#ifdef GL_ES
precision highp float;
#endif

//attributes from vertex shader
varying vec4 vColor;
varying vec3 vNormal;

//our texture samplers
uniform sampler2D u_texture0;   //diffuse map
uniform sampler2D u_normals;   //normal map

//uniform vec3 LightPos;     //light position, normalized
varying vec3 fragPosition;


// camera position untransformed for specular lighting
uniform vec3 CameraPosWorldSpace;

// lighting variables
varying vec3 FragPositionWorldSpace;
varying vec3 LightPosWorldSpace;
varying vec3 LightPosFromVertex;




// TODO: there are probably some redundant variables between this and the vertex shader, including unnecessary copies from vertex to fragment, where we could instead just pass from app to fragment
// TODO: there are probably some redundant variables between this and the vertex shader, including unnecessary copies from vertex to fragment, where we could instead just pass from app to fragment
// TODO: there are probably some redundant variables between this and the vertex shader, including unnecessary copies from vertex to fragment, where we could instead just pass from app to fragment
// TODO: there are probably some redundant variables between this and the vertex shader, including unnecessary copies from vertex to fragment, where we could instead just pass from app to fragment
// TODO: there are probably some redundant variables between this and the vertex shader, including unnecessary copies from vertex to fragment, where we could instead just pass from app to fragment
// TODO: there are probably some redundant variables between this and the vertex shader, including unnecessary copies from vertex to fragment, where we could instead just pass from app to fragment
// TODO: there are probably some redundant variables between this and the vertex shader, including unnecessary copies from vertex to fragment, where we could instead just pass from app to fragment
// TODO: there are probably some redundant variables between this and the vertex shader, including unnecessary copies from vertex to fragment, where we could instead just pass from app to fragment
// TODO: there are probably some redundant variables between this and the vertex shader, including unnecessary copies from vertex to fragment, where we could instead just pass from app to fragment
// TODO: there are probably some redundant variables between this and the vertex shader, including unnecessary copies from vertex to fragment, where we could instead just pass from app to fragment
// TODO: there are probably some redundant variables between this and the vertex shader, including unnecessary copies from vertex to fragment, where we could instead just pass from app to fragment
// TODO: there are probably some redundant variables between this and the vertex shader, including unnecessary copies from vertex to fragment, where we could instead just pass from app to fragment
// TODO: there are probably some redundant variables between this and the vertex shader, including unnecessary copies from vertex to fragment, where we could instead just pass from app to fragment
// TODO: there are probably some redundant variables between this and the vertex shader, including unnecessary copies from vertex to fragment, where we could instead just pass from app to fragment
// TODO: there are probably some redundant variables between this and the vertex shader, including unnecessary copies from vertex to fragment, where we could instead just pass from app to fragment
void main() {

    vec4 finalColor = vColor;

    float Sum = 0.0;
    float diffuseLighting;

    float amplitude = __AMPLITUDE__FACTOR__ / pow ( (distance(LightPosWorldSpace, FragPositionWorldSpace)) , 2.0) * 2.5;

    // ambient lighting
//    Sum += 0.00;

    // diffuse lighting
    vec3 backToLightSource = normalize(LightPosWorldSpace - FragPositionWorldSpace);
    diffuseLighting = dot(backToLightSource, normalize(vNormal)) * amplitude * 0.85;
    Sum += diffuseLighting;

    gl_FragColor = finalColor * Sum;
}



