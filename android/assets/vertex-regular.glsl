//our attributes
attribute vec3 a_position;
attribute vec4 a_color;
attribute vec3 a_normal;
attribute vec2 a_texCoords;


//our camera matrix
uniform mat4 u_projTrans;


// our light
uniform vec3 LightPos;


//send the color out to the fragment shader
varying vec4 vColor;
varying vec3 vNormal;
varying vec3 fragPosition;
varying vec2 texCoord;

// send the light
varying vec3 LightPosFromVertex;

// send the frag position untransformed
varying vec3 FragPositionWorldSpace;

// send the light untransformed
varying vec3 LightPosWorldSpace;



void main() {

	vColor = a_color;
    vNormal = a_normal;


	gl_Position = u_projTrans * vec4(a_position.xyz, 1.0);
	// HACK TO MAKE SURE THAT OBJECTS APPEAR CORRECT IN TERMS OF X COORDS... NOT REAL SOLUTION. WITHOUT THIS, NEGATIVE X COORDS VERTICES GO TO THE RIGHT OF THE SCREEN...
	gl_Position.x *= -1.0;
	fragPosition = gl_Position.xyz;


	LightPosFromVertex = (u_projTrans * vec4(LightPos, 1.0)).xyz;
	LightPosFromVertex.x *= -1.0;



	texCoord = a_texCoords;

	// untransformed frag position for lighting
	FragPositionWorldSpace = a_position;
	FragPositionWorldSpace.x *= -1.0;

	// untransformed light position for lighting
	LightPosWorldSpace = LightPos;
	LightPosWorldSpace.x *= -1.0;

	vNormal.x *= -1.0;

}