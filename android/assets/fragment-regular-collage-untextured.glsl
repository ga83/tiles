#ifdef GL_ES
precision highp float;
#endif

//attributes from vertex shader
varying vec4 vColor;
varying vec3 vNormal;

//uniform vec3 LightPos;     //light position, normalized
varying vec3 fragPosition;


void main() {

    vec4 finalColor = vec4(0.8,0.8,0.8,1.0);
    vec3 LightPos   = vec3(0.0,0.0,-500.0);

    float Sum = 0.0;
    float amplitude = __AMPLITUDE__FACTOR__ / pow ( (distance(LightPos,fragPosition)) , 2.0);

    // ambient lighting
     Sum += 0.33;

    // diffuse lighting
    vec3 backToLightSource = normalize(LightPos - fragPosition);
    float diffuseLighting = dot(backToLightSource, normalize(vNormal)) * 0.66 * amplitude;
    Sum += diffuseLighting;

    gl_FragColor = finalColor * Sum;
}



