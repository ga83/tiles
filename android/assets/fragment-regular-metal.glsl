#ifdef GL_ES
precision highp float;
#endif

//attributes from vertex shader
varying vec4 vColor;
varying vec3 vNormal;

//our texture samplers
uniform sampler2D u_texture0;   //diffuse map
uniform sampler2D u_normals;   //normal map

//uniform vec3 LightPos;     //light position, normalized
varying vec3 fragPosition;


// camera position untransformed for specular lighting
uniform vec3 CameraPosWorldSpace;

// lighting variables
varying vec3 FragPositionWorldSpace;
varying vec3 LightPosWorldSpace;
varying vec3 LightPosFromVertex;


// TODO: there are probably some redundant variables between this and the vertex shader, including unnecessary copies from vertex to fragment, where we could instead just pass from app to fragment
// TODO: there are probably some redundant variables between this and the vertex shader, including unnecessary copies from vertex to fragment, where we could instead just pass from app to fragment
// TODO: there are probably some redundant variables between this and the vertex shader, including unnecessary copies from vertex to fragment, where we could instead just pass from app to fragment
// TODO: there are probably some redundant variables between this and the vertex shader, including unnecessary copies from vertex to fragment, where we could instead just pass from app to fragment
// TODO: there are probably some redundant variables between this and the vertex shader, including unnecessary copies from vertex to fragment, where we could instead just pass from app to fragment
// TODO: there are probably some redundant variables between this and the vertex shader, including unnecessary copies from vertex to fragment, where we could instead just pass from app to fragment
// TODO: there are probably some redundant variables between this and the vertex shader, including unnecessary copies from vertex to fragment, where we could instead just pass from app to fragment
// TODO: there are probably some redundant variables between this and the vertex shader, including unnecessary copies from vertex to fragment, where we could instead just pass from app to fragment
// TODO: there are probably some redundant variables between this and the vertex shader, including unnecessary copies from vertex to fragment, where we could instead just pass from app to fragment
// TODO: there are probably some redundant variables between this and the vertex shader, including unnecessary copies from vertex to fragment, where we could instead just pass from app to fragment
// TODO: there are probably some redundant variables between this and the vertex shader, including unnecessary copies from vertex to fragment, where we could instead just pass from app to fragment
// TODO: there are probably some redundant variables between this and the vertex shader, including unnecessary copies from vertex to fragment, where we could instead just pass from app to fragment
// TODO: there are probably some redundant variables between this and the vertex shader, including unnecessary copies from vertex to fragment, where we could instead just pass from app to fragment
// TODO: there are probably some redundant variables between this and the vertex shader, including unnecessary copies from vertex to fragment, where we could instead just pass from app to fragment
// TODO: there are probably some redundant variables between this and the vertex shader, including unnecessary copies from vertex to fragment, where we could instead just pass from app to fragment
void main() {

    vec4 finalColor = vColor;

    float Sum = 0.0;
    float specularLighting;
    float amplitude = __AMPLITUDE__FACTOR__ / pow ( (distance(LightPosWorldSpace, FragPositionWorldSpace)) , 2.0) * 4.5;

    vec3 CameraPosWorldSpace1 = CameraPosWorldSpace;
    CameraPosWorldSpace1.x *= -1.0;

    // ambient lighting
//    Sum += 0.15;

/*
    // specular
    vec3 fromLightSource = normalize(fragPosition - LightPos);
    vec3 backToEye = normalize(vec3(0,0,0) - fragPosition);
    float dot = dot(reflect(fromLightSource, vNormal ), backToEye);
    specularLighting = pow(dot, 2.0) * float(dot > 0.0) * amplitude * 0.85;
*/

    // specular
    vec3 fromLightSourceToFragpos = normalize(FragPositionWorldSpace - LightPosWorldSpace);
    vec3 fromFragposToCamera = normalize(CameraPosWorldSpace1 - FragPositionWorldSpace);
    float dot = dot(reflect(fromLightSourceToFragpos, vNormal ), fromFragposToCamera);
    specularLighting = pow(dot, 2.0) * float(dot > 0.0) * amplitude ;



    Sum += specularLighting;


    gl_FragColor = finalColor * Sum;
}



