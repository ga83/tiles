#ifdef GL_ES
precision highp float;
#endif

//attributes from vertex shader
varying vec4 vColor;
varying vec3 vNormal;

//our texture samplers
uniform sampler2D u_texture0;   //diffuse map
uniform sampler2D u_normals;   //normal map

uniform vec3 LightPos;     //light position, normalized
varying vec3 fragPosition;

// for outlining
varying vec3 vBC;			// input, custom for outlining polys


void main() {

    vec4 finalColor = vColor;

    float diffuseLighting;
    float amplitude = __AMPLITUDE__FACTOR__ / pow ( (distance(LightPos,fragPosition)) , 2.0) * 3.0;

    // ambient lighting
    float ambient = 0.6;

    // diffuse lighting
    vec3 backToLightSource = normalize(LightPos - fragPosition);
    diffuseLighting = dot(backToLightSource, normalize(vNormal)) * amplitude * 0.4;

    float minimum = min(min(vBC[0], vBC[1]), vBC[2]);

    if(minimum < 0.012) {
        gl_FragColor = finalColor * (diffuseLighting + ambient) * 1.5;
    } else {
        gl_FragColor = finalColor * (diffuseLighting + ambient) * 0.75;
    }
}



